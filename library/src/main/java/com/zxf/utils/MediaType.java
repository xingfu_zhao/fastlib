package com.zxf.utils;


import java.util.ArrayList;
import java.util.List;

public class MediaType {
    public static final String FILE_TYPE_UNKNOWN = "unknown";
    public static final String FILE_TYPE_VIDEO = "video";
    public static final String FILE_TYPE_AUDIO = "audio";
    public static final String FILE_TYPE_IMAGE = "image";
    public static final String FILE_TYPE_TEXT = "text";
    public static final String FILE_TYPE_APK = "apk";
    public static final String FILE_TYPE_ZIP = "zip";
    public static final String FILE_TYPE_PDF = "pdf";
    public static final String FILE_TYPE_XML = "xml";
    public static final String FILE_TYPE_XLS = "xls";//excel
    public static final String FILE_TYPE_DOC = "doc";//word
    public static final String FILE_TYPE_PPT = "ppt";//PPT

    private MediaType(String suffix, String mimeType, String fileType) {
        this.suffix = suffix;
        this.mimeType = mimeType;
        this.fileType = fileType;
    }


    public String fileType;//文件类型
    public String suffix;//后缀名
    public String mimeType;//文件操作类型

    @Override
    public String toString() {
        return "MediaType{" +
                "fileType=" + fileType +
                ", suffix='" + suffix + '\'' +
                ", mimeType='" + mimeType + '\'' +
                '}';
    }

    public String getFileType() {
        return fileType;
    }

    public String getSuffix() {
        return suffix;
    }


    public String getMimeType() {
        return mimeType;
    }

    public boolean isSuffix(String suffix) {
        return suffix == null ? false : suffix.equals(this.suffix);
    }

    public boolean isMineType(String mimeType) {
        return mimeType == null ? false : mimeType.equals(this.mimeType);
    }

    public static MediaType getMimeType(String suffix, String mimeType) {
        if (mimeType == null || mimeType.length() <= 0) {
            return fromSuffix(suffix).get(0);
        }
        if (suffix == null || suffix.length() <= 0) {
            return new MediaType("", mimeType, FILE_TYPE_UNKNOWN);
        }
        List<MediaType> suffixTypes = fromSuffix(suffix);
        for (MediaType mediaType : suffixTypes) {
            if (mimeType.equals(mediaType.mimeType)) {
                return mediaType;
            }
        }
        return new MediaType(suffix, mimeType, FILE_TYPE_UNKNOWN);
    }

    public static List<MediaType> fromSuffix(String suffix) {
        List<MediaType> result = new ArrayList<>();
        if (suffix == null || suffix.length() <= 0) {
            result.add(new MediaType("", "*/*", FILE_TYPE_UNKNOWN));
            return result;
        }
        switch (suffix) {
            case ".wbmp":
                result.add(new MediaType(suffix, "image/vnd.wap.wbmp", FILE_TYPE_IMAGE));
                break;
            case ".tif":
                result.add(new MediaType(suffix, "image/tiff", FILE_TYPE_IMAGE));
                result.add(new MediaType(suffix, "application/x-tif", FILE_TYPE_IMAGE));
                break;
            case ".tiff":
                result.add(new MediaType(suffix, "image/tiff", FILE_TYPE_IMAGE));
                break;
            case ".rp":
                result.add(new MediaType(suffix, "image/vnd.rn-realpix", FILE_TYPE_IMAGE));
                break;
            case ".png":
                result.add(new MediaType(suffix, "image/png", FILE_TYPE_IMAGE));
                result.add(new MediaType(suffix, "application/x-png", FILE_TYPE_IMAGE));
                break;
            case ".net":
                result.add(new MediaType(suffix, "image/pnetvue", FILE_TYPE_IMAGE));
                break;
            case ".jpg":
                result.add(new MediaType(suffix, "image/jpeg", FILE_TYPE_IMAGE));
                result.add(new MediaType(suffix, "application/x-jpg", FILE_TYPE_IMAGE));
                break;
            case ".jpeg":
            case ".jpe":
                result.add(new MediaType(suffix, "image/jpeg", FILE_TYPE_IMAGE));
                result.add(new MediaType(suffix, "application/x-jpe", FILE_TYPE_IMAGE));
                break;
            case ".jfif":
                result.add(new MediaType(suffix, "image/jpeg", FILE_TYPE_IMAGE));
                break;
            case ".ico":
                result.add(new MediaType(suffix, "image/x-icon", FILE_TYPE_IMAGE));
                result.add(new MediaType(suffix, "application/x-ico", FILE_TYPE_IMAGE));
                break;
            case ".gif":
                result.add(new MediaType(suffix, "image/gif", FILE_TYPE_IMAGE));
                break;
            case ".fax":
                result.add(new MediaType(suffix, "image/fax", FILE_TYPE_IMAGE));
                break;
            case ".wvx":
                result.add(new MediaType(suffix, "video/x-ms-wvx", FILE_TYPE_VIDEO));
                break;
            case ".wmx":
                result.add(new MediaType(suffix, "video/x-ms-wmx", FILE_TYPE_VIDEO));
                break;
            case ".wmv":
                result.add(new MediaType(suffix, "video/x-ms-wmv", FILE_TYPE_VIDEO));
                break;
            case ".wm":
                result.add(new MediaType(suffix, "video/x-ms-wm", FILE_TYPE_VIDEO));
                break;
            case ".mpv2":
            case ".mp2v":
                result.add(new MediaType(suffix, "video/mpeg", FILE_TYPE_VIDEO));
                break;
            case ".mpv":
            case ".mpeg":
            case ".mpg ":
                result.add(new MediaType(suffix, "video/mpg", FILE_TYPE_VIDEO));
                break;
            case ".mps":
            case ".mpe":
            case ".m2v":
            case ".m1v":
                result.add(new MediaType(suffix, "video/x-mpeg", FILE_TYPE_VIDEO));
                break;
            case ".mpa":
                result.add(new MediaType(suffix, "video/x-mpg", FILE_TYPE_VIDEO));
                break;
            case ".mp4":
                result.add(new MediaType(suffix, "video/mpeg4", FILE_TYPE_VIDEO));
                result.add(new MediaType(suffix, "audio/mp4", FILE_TYPE_AUDIO));
                break;
            case ".m4e":
            case ".m3u8":
                result.add(new MediaType(suffix, "video/mpeg4", FILE_TYPE_VIDEO));
                break;
            case ".movie":
                result.add(new MediaType(suffix, "video/x-sgi-movie", FILE_TYPE_VIDEO));
                break;
            case ".IVF":
                result.add(new MediaType(suffix, "video/x-ivf", FILE_TYPE_VIDEO));
                break;
            case ".avi":
                result.add(new MediaType(suffix, "video/avi", FILE_TYPE_VIDEO));
                break;
            case ".asx":
            case ".asf":
                result.add(new MediaType(suffix, "video/x-ms-asf", FILE_TYPE_VIDEO));
                break;
            case ".wax":
                result.add(new MediaType(suffix, "audio/x-ms-wax", FILE_TYPE_AUDIO));
                break;
            case ".wav":
                result.add(new MediaType(suffix, "audio/wav", FILE_TYPE_AUDIO));
                break;
            case ".au":
            case ".snd":
                result.add(new MediaType(suffix, "audio/basic", FILE_TYPE_AUDIO));
                break;
            case ".rmm":
            case ".ram":
                result.add(new MediaType(suffix, "audio/x-pn-realaudio", FILE_TYPE_AUDIO));
                break;
            case ".midi":
            case ".mid":
            case ".rmi":
                result.add(new MediaType(suffix, "audio/mid", FILE_TYPE_AUDIO));
                break;
            case ".pls":
            case ".xpl":
                result.add(new MediaType(suffix, "audio/scpls", FILE_TYPE_AUDIO));
                break;
            case ".mpga":
                result.add(new MediaType(suffix, "audio/rn-mpeg", FILE_TYPE_AUDIO));
                break;
            case ".mns":
                result.add(new MediaType(suffix, "audio/x-musicnet-stream", FILE_TYPE_AUDIO));
                break;
            case ".wma":
                result.add(new MediaType(suffix, "audio/x-ms-wma", FILE_TYPE_AUDIO));
                break;
            case ".mp1":
                result.add(new MediaType(suffix, "audio/mp1", FILE_TYPE_AUDIO));
                break;
            case ".mnd":
                result.add(new MediaType(suffix, "audio/x-musicnet-download", FILE_TYPE_AUDIO));
                break;
            case ".mp3":
                result.add(new MediaType(suffix, "audio/mp3", FILE_TYPE_AUDIO));
                break;
            case ".mp2":
                result.add(new MediaType(suffix, "audio/mp2", FILE_TYPE_AUDIO));
                break;
            case ".m3u":
                result.add(new MediaType(suffix, "audio/mpegurl", FILE_TYPE_AUDIO));
                break;
            case ".lmsff":
                result.add(new MediaType(suffix, "audio/x-la-lms", FILE_TYPE_AUDIO));
                break;
            case ".lavs":
                result.add(new MediaType(suffix, "audio/x-liquid-secure", FILE_TYPE_AUDIO));
                break;
            case ".la1":
                result.add(new MediaType(suffix, "audio/x-liquid-file", FILE_TYPE_AUDIO));
                break;
            case ".aifc":
            case ".aiff":
            case ".aif":
                result.add(new MediaType(suffix, "audio/aiff", FILE_TYPE_AUDIO));
                break;
            case ".acp":
                result.add(new MediaType(suffix, "audio/x-mei-aac", FILE_TYPE_AUDIO));
                break;
            case ".txt":
            case ".sor":
            case ".sol":
                result.add(new MediaType(suffix, "text/plain", FILE_TYPE_TEXT));
                break;
            case ".r3t":
                result.add(new MediaType(suffix, "text/vnd.rn-realtext3d", FILE_TYPE_TEXT));
                break;
            case ".odc":
                result.add(new MediaType(suffix, "text/x-ms-odc", FILE_TYPE_TEXT));
                break;
            case ".html":
            case ".stm":
            case ".plg":
            case ".jsp":
            case ".htm":
            case ".htx":
                result.add(new MediaType(suffix, "text/html", FILE_TYPE_TEXT));
                break;
            case ".xsl":
                result.add(new MediaType(suffix, "text/xml", FILE_TYPE_XLS));
                break;
            case ".xquery":
            case ".xq":
            case ".xslt":
            case ".xsd":
            case ".xql":
            case ".xdr":
            case ".wsdl":
            case ".vxml":
            case ".tsd":
            case ".vml":
            case ".tld":
            case ".svg":
            case ".spp":
            case ".mtx":
            case ".mml":
            case ".math":
            case ".fo":
            case ".ent":
            case ".dcd":
            case ".cml":
            case ".biz":
                result.add(new MediaType(suffix, "text/xml", FILE_TYPE_UNKNOWN));
                break;
            case ".wsc":
                result.add(new MediaType(suffix, "text/scriptlet", FILE_TYPE_UNKNOWN));
                break;
            case ".wml":
                result.add(new MediaType(suffix, "text/vnd.wap.wml", FILE_TYPE_TEXT));
                break;
            case ".vcf":
                result.add(new MediaType(suffix, "text/x-vcard", FILE_TYPE_TEXT));
                break;
            case ".uls":
                result.add(new MediaType(suffix, "text/iuls", FILE_TYPE_TEXT));
                break;
            case ".rt":
                result.add(new MediaType(suffix, "text/vnd.rn-realtext", FILE_TYPE_TEXT));
                break;
            case ".htt":
                result.add(new MediaType(suffix, "text/webviewhtml", FILE_TYPE_TEXT));
                break;
            case ".htc":
                result.add(new MediaType(suffix, "text/x-component", FILE_TYPE_TEXT));
                break;
            case ".csv":
                result.add(new MediaType(suffix, "text/csv", FILE_TYPE_TEXT));
                break;
            case ".css":
                result.add(new MediaType(suffix, "text/css", FILE_TYPE_TEXT));
                break;
            case ".asp":
                result.add(new MediaType(suffix, "text/asp", FILE_TYPE_TEXT));
                break;
            case ".asa":
                result.add(new MediaType(suffix, "text/asa", FILE_TYPE_TEXT));
                break;
            case ".323":
                result.add(new MediaType(suffix, "text/h323", FILE_TYPE_TEXT));
                break;
            case ".apk":
                result.add(new MediaType(suffix, "application/vnd.android.package-archive", FILE_TYPE_APK));
                break;
            case ".pdf":
                result.add(new MediaType(suffix, "application/pdf", FILE_TYPE_PDF));
                break;
            case ".ai":
                result.add(new MediaType(suffix, "application/postscript", FILE_TYPE_UNKNOWN));
                break;
            case ".eps":
            case ".ps":
                result.add(new MediaType(suffix, "application/postscript", FILE_TYPE_UNKNOWN));
                result.add(new MediaType(suffix, "application/x-ps", FILE_TYPE_UNKNOWN));
                break;
            case ".xml":
                result.add(new MediaType(suffix, "application/xml", FILE_TYPE_XML));
                result.add(new MediaType(suffix, "application/atom+xml", FILE_TYPE_XML));
                result.add(new MediaType(suffix, "application/rss+xml", FILE_TYPE_XML));
                result.add(new MediaType(suffix, "application/soap+xml", FILE_TYPE_XML));
                result.add(new MediaType(suffix, "application/xop+xml", FILE_TYPE_XML));
                result.add(new MediaType(suffix, "text/xml", FILE_TYPE_XML));
                break;
            case ".js":
                result.add(new MediaType(suffix, "application/ecmascript", FILE_TYPE_UNKNOWN));
                result.add(new MediaType(suffix, "application/javascript", FILE_TYPE_UNKNOWN));
                result.add(new MediaType(suffix, "application/x-javascript", FILE_TYPE_UNKNOWN));
                break;
            case ".edi":
                result.add(new MediaType(suffix, "application/EDI-X12", FILE_TYPE_UNKNOWN));
                result.add(new MediaType(suffix, "application/EDIFACT", FILE_TYPE_UNKNOWN));
                break;
            case ".json":
                result.add(new MediaType(suffix, "application/json", FILE_TYPE_UNKNOWN));
                break;
            case ".ogg":
                result.add(new MediaType(suffix, "application/ogg", FILE_TYPE_UNKNOWN));
                break;
            case ".rdf":
                result.add(new MediaType(suffix, "application/rdf+xml", FILE_TYPE_UNKNOWN));
                result.add(new MediaType(suffix, "text/xml", FILE_TYPE_UNKNOWN));
                break;
            case ".woff":
                result.add(new MediaType(suffix, "application/font-woff", FILE_TYPE_UNKNOWN));
                break;
            case ".xhtml":
                result.add(new MediaType(suffix, "application/xhtml+xml", FILE_TYPE_UNKNOWN));
                result.add(new MediaType(suffix, "text/html", FILE_TYPE_UNKNOWN));
                break;
            case ".dtd":
                result.add(new MediaType(suffix, "application/xml-dtd", FILE_TYPE_UNKNOWN));
                result.add(new MediaType(suffix, "text/xml", FILE_TYPE_UNKNOWN));
                break;
            case ".zip":
                result.add(new MediaType(suffix, "application/zip", FILE_TYPE_ZIP));
                break;
            case ".gzip":
                result.add(new MediaType(suffix, "application/gzip", FILE_TYPE_ZIP));
                break;
            case ".xls":
                result.add(new MediaType(suffix, "application/vnd.ms-excel", FILE_TYPE_XLS));
                result.add(new MediaType(suffix, "application/x-xls", FILE_TYPE_XLS));
                break;
            case ".001":
                result.add(new MediaType(suffix, "application/x-001", FILE_TYPE_UNKNOWN));
                break;
            case ".301":
                result.add(new MediaType(suffix, "application/x-301", FILE_TYPE_UNKNOWN));
                break;
            case ".906":
                result.add(new MediaType(suffix, "application/x-906", FILE_TYPE_UNKNOWN));
                break;
            case ".a11":
                result.add(new MediaType(suffix, "application/x-a11", FILE_TYPE_UNKNOWN));
                break;
            case ".awf":
                result.add(new MediaType(suffix, "application/vnd.adobe.workflow", FILE_TYPE_UNKNOWN));
                break;
            case ".bmp":
                result.add(new MediaType(suffix, "application/x-bmp", FILE_TYPE_UNKNOWN));
                break;
            case ".c4t":
                result.add(new MediaType(suffix, "application/x-c4t", FILE_TYPE_UNKNOWN));
                break;
            case ".cal":
                result.add(new MediaType(suffix, "application/x-cals", FILE_TYPE_UNKNOWN));
                break;
            case ".cdf":
                result.add(new MediaType(suffix, "application/x-netcdf", FILE_TYPE_UNKNOWN));
                break;
            case ".cel":
                result.add(new MediaType(suffix, "application/x-cel", FILE_TYPE_UNKNOWN));
                break;
            case ".cg4":
            case ".g4":
            case ".ig4":
                result.add(new MediaType(suffix, "application/x-g4", FILE_TYPE_UNKNOWN));
                break;
            case ".cit":
                result.add(new MediaType(suffix, "application/x-cit", FILE_TYPE_UNKNOWN));
                break;
            case ".bot":
                result.add(new MediaType(suffix, "application/x-bot", FILE_TYPE_UNKNOWN));
                break;
            case ".c90":
                result.add(new MediaType(suffix, "application/x-c90", FILE_TYPE_UNKNOWN));
                break;
            case ".cat":
                result.add(new MediaType(suffix, "application/vnd.ms-pki.seccat", FILE_TYPE_UNKNOWN));
                break;
            case ".cdr":
                result.add(new MediaType(suffix, "application/x-cdr", FILE_TYPE_UNKNOWN));
                break;
            case ".cer":
            case ".crt":
            case ".der":
                result.add(new MediaType(suffix, "application/x-x509-ca-cert", FILE_TYPE_UNKNOWN));
                break;
            case ".cgm":
                result.add(new MediaType(suffix, "application/x-cgm", FILE_TYPE_UNKNOWN));
                break;
            case ".cmx":
                result.add(new MediaType(suffix, "application/x-cmx", FILE_TYPE_UNKNOWN));
                break;
            case ".crl":
                result.add(new MediaType(suffix, "application/pkix-crl", FILE_TYPE_UNKNOWN));
                break;
            case ".csi":
                result.add(new MediaType(suffix, "application/x-csi", FILE_TYPE_UNKNOWN));
                break;
            case ".cut":
                result.add(new MediaType(suffix, "application/x-cut", FILE_TYPE_UNKNOWN));
                break;
            case ".dbm":
                result.add(new MediaType(suffix, "application/x-dbm", FILE_TYPE_UNKNOWN));
                break;
            case ".cmp":
                result.add(new MediaType(suffix, "application/x-cmp", FILE_TYPE_UNKNOWN));
                break;
            case ".cot":
                result.add(new MediaType(suffix, "application/x-cot", FILE_TYPE_UNKNOWN));
                break;
            case ".dbf":
                result.add(new MediaType(suffix, "application/x-dbf", FILE_TYPE_UNKNOWN));
                break;
            case ".dbx":
                result.add(new MediaType(suffix, "application/x-dbx", FILE_TYPE_UNKNOWN));
                break;
            case ".dcx":
                result.add(new MediaType(suffix, "application/x-dcx", FILE_TYPE_UNKNOWN));
                break;
            case ".dgn":
                result.add(new MediaType(suffix, "application/x-dgn", FILE_TYPE_UNKNOWN));
                break;
            case ".dll":
            case ".exe":
                result.add(new MediaType(suffix, "application/x-msdownload", FILE_TYPE_UNKNOWN));
                break;
            case ".dib":
                result.add(new MediaType(suffix, "application/x-dib", FILE_TYPE_UNKNOWN));
                break;
            case ".doc":
                result.add(new MediaType(suffix, "application/msword", FILE_TYPE_DOC));
                break;
            case ".rtf":
                result.add(new MediaType(suffix, "application/x-rtf", FILE_TYPE_UNKNOWN));
                result.add(new MediaType(suffix, "application/msword", FILE_TYPE_UNKNOWN));
                break;
            case ".dot":
            case ".wiz":
                result.add(new MediaType(suffix, "application/msword", FILE_TYPE_UNKNOWN));
                break;
            case ".drw":
                result.add(new MediaType(suffix, "application/x-drw", FILE_TYPE_UNKNOWN));
                break;
            case ".dwf":
                result.add(new MediaType(suffix, "application/x-dwf", FILE_TYPE_UNKNOWN));
                result.add(new MediaType(suffix, "Model/vnd.dwf", FILE_TYPE_UNKNOWN));
                break;
            case ".dxb":
                result.add(new MediaType(suffix, "application/x-dxb", FILE_TYPE_UNKNOWN));
                break;
            case ".edn":
                result.add(new MediaType(suffix, "application/vnd.adobe.edn", FILE_TYPE_UNKNOWN));
                break;
            case ".dwg":
                result.add(new MediaType(suffix, "application/x-dwg", FILE_TYPE_UNKNOWN));
                break;
            case ".dxf":
                result.add(new MediaType(suffix, "application/x-dxf", FILE_TYPE_UNKNOWN));
                break;
            case ".emf":
                result.add(new MediaType(suffix, "application/x-emf", FILE_TYPE_UNKNOWN));
                break;
            case ".epi":
                result.add(new MediaType(suffix, "application/x-epi", FILE_TYPE_UNKNOWN));
                break;
            case ".fdf":
                result.add(new MediaType(suffix, "application/vnd.fdf", FILE_TYPE_UNKNOWN));
                break;
            case ".etd":
                result.add(new MediaType(suffix, "application/x-ebx", FILE_TYPE_UNKNOWN));
                break;
            case ".fif":
                result.add(new MediaType(suffix, "application/fractals", FILE_TYPE_UNKNOWN));
                break;
            case ".frm":
                result.add(new MediaType(suffix, "application/x-frm", FILE_TYPE_UNKNOWN));
                break;
            case ".gbr":
                result.add(new MediaType(suffix, "application/x-gbr", FILE_TYPE_UNKNOWN));
                break;
            case ".gl2":
                result.add(new MediaType(suffix, "application/x-gl2", FILE_TYPE_UNKNOWN));
                break;
            case ".hgl":
                result.add(new MediaType(suffix, "application/x-hgl", FILE_TYPE_UNKNOWN));
                break;
            case ".hpg":
                result.add(new MediaType(suffix, "application/x-hpgl", FILE_TYPE_UNKNOWN));
                break;
            case ".hqx":
                result.add(new MediaType(suffix, "application/mac-binhex40", FILE_TYPE_UNKNOWN));
                break;
            case ".hta":
                result.add(new MediaType(suffix, "application/hta", FILE_TYPE_UNKNOWN));
                break;
            case ".gp4":
                result.add(new MediaType(suffix, "application/x-gp4", FILE_TYPE_UNKNOWN));
                break;
            case ".hmr":
                result.add(new MediaType(suffix, "application/x-hmr", FILE_TYPE_UNKNOWN));
                break;
            case ".hpl":
                result.add(new MediaType(suffix, "application/x-hpl", FILE_TYPE_UNKNOWN));
                break;
            case ".hrf":
                result.add(new MediaType(suffix, "application/x-hrf", FILE_TYPE_UNKNOWN));
                break;
            case ".icb":
                result.add(new MediaType(suffix, "application/x-icb", FILE_TYPE_UNKNOWN));
                break;
            case ".iii":
                result.add(new MediaType(suffix, "application/x-iphone", FILE_TYPE_UNKNOWN));
                break;
            case ".ins":
            case ".isp":
                result.add(new MediaType(suffix, "application/x-internet-signup", FILE_TYPE_UNKNOWN));
                break;
            case ".iff":
                result.add(new MediaType(suffix, "application/x-iff", FILE_TYPE_UNKNOWN));
                break;
            case ".igs":
                result.add(new MediaType(suffix, "application/x-igs", FILE_TYPE_UNKNOWN));
                break;
            case ".img":
                result.add(new MediaType(suffix, "application/x-img", FILE_TYPE_UNKNOWN));
                break;
            case ".ls":
            case ".mocha":
                result.add(new MediaType(suffix, "application/x-javascript", FILE_TYPE_UNKNOWN));
                break;
            case ".lar":
                result.add(new MediaType(suffix, "application/x-laplayer-reg", FILE_TYPE_UNKNOWN));
                break;
            case ".latex":
                result.add(new MediaType(suffix, "application/x-latex", FILE_TYPE_UNKNOWN));
                break;
            case ".lbm":
                result.add(new MediaType(suffix, "application/x-lbm", FILE_TYPE_UNKNOWN));
                break;
            case ".ltr":
                result.add(new MediaType(suffix, "application/x-ltr", FILE_TYPE_UNKNOWN));
                break;
            case ".man":
                result.add(new MediaType(suffix, "application/x-troff-man", FILE_TYPE_UNKNOWN));
                break;
            case ".mdb":
                result.add(new MediaType(suffix, "application/msaccess", FILE_TYPE_UNKNOWN));
                result.add(new MediaType(suffix, "application/x-mdb", FILE_TYPE_UNKNOWN));
                break;
            case ".mac":
                result.add(new MediaType(suffix, "application/x-mac", FILE_TYPE_UNKNOWN));
                break;
            case ".mfp":
            case ".swf":
                result.add(new MediaType(suffix, "application/x-shockwave-flash", FILE_TYPE_UNKNOWN));
                break;
            case ".mi":
                result.add(new MediaType(suffix, "application/x-mi", FILE_TYPE_UNKNOWN));
                break;
            case ".mil":
                result.add(new MediaType(suffix, "application/x-mil", FILE_TYPE_UNKNOWN));
                break;
            case ".mpd":
            case ".mpp":
            case ".mpt":
            case ".mpw":
            case ".mpx":
                result.add(new MediaType(suffix, "application/vnd.ms-project", FILE_TYPE_UNKNOWN));
                break;
            case ".mxp":
                result.add(new MediaType(suffix, "application/x-mmxp", FILE_TYPE_UNKNOWN));
                break;
            case ".nrf":
                result.add(new MediaType(suffix, "application/x-nrf", FILE_TYPE_UNKNOWN));
                break;
            case ".out":
                result.add(new MediaType(suffix, "application/x-out", FILE_TYPE_UNKNOWN));
                break;
            case ".p12":
            case ".pfx":
                result.add(new MediaType(suffix, "application/x-pkcs12", FILE_TYPE_UNKNOWN));
                break;
            case ".p7c":
            case ".p7m":
                result.add(new MediaType(suffix, "application/pkcs7-mime", FILE_TYPE_UNKNOWN));
                break;
            case ".p7r":
                result.add(new MediaType(suffix, "application/x-pkcs7-certreqresp", FILE_TYPE_UNKNOWN));
                break;
            case ".pc5":
                result.add(new MediaType(suffix, "application/x-pc5", FILE_TYPE_UNKNOWN));
                break;
            case ".pcl":
                result.add(new MediaType(suffix, "application/x-pcl", FILE_TYPE_UNKNOWN));
                break;
            case ".pdx":
                result.add(new MediaType(suffix, "application/vnd.adobe.pdx", FILE_TYPE_UNKNOWN));
                break;
            case ".pgl":
                result.add(new MediaType(suffix, "application/x-pgl", FILE_TYPE_UNKNOWN));
                break;
            case ".pko":
                result.add(new MediaType(suffix, "application/vnd.ms-pki.pko", FILE_TYPE_UNKNOWN));
                break;
            case ".p10":
                result.add(new MediaType(suffix, "application/pkcs10", FILE_TYPE_UNKNOWN));
                break;
            case ".p7b":
            case ".spc":
                result.add(new MediaType(suffix, "application/x-pkcs7-certificates", FILE_TYPE_UNKNOWN));
                break;
            case ".p7s":
                result.add(new MediaType(suffix, "application/pkcs7-signature", FILE_TYPE_UNKNOWN));
                break;
            case ".pci":
                result.add(new MediaType(suffix, "application/x-pci", FILE_TYPE_UNKNOWN));
                break;
            case ".pcx":
                result.add(new MediaType(suffix, "application/x-pcx", FILE_TYPE_UNKNOWN));
                break;
            case ".pic":
                result.add(new MediaType(suffix, "application/x-pic", FILE_TYPE_UNKNOWN));
                break;
            case ".pl":
                result.add(new MediaType(suffix, "application/x-perl", FILE_TYPE_UNKNOWN));
                break;
            case ".plt":
                result.add(new MediaType(suffix, "application/x-plt", FILE_TYPE_UNKNOWN));
                break;
            case ".ppa":
            case ".pps":
            case ".pwz":
            case ".pot":
                result.add(new MediaType(suffix, "application/vnd.ms-powerpoint", FILE_TYPE_UNKNOWN));
                break;
            case ".ppt":
                result.add(new MediaType(suffix, "application/vnd.ms-powerpoint", FILE_TYPE_PPT));
                result.add(new MediaType(suffix, "application/x-ppt", FILE_TYPE_PPT));
                break;
            case ".prf":
                result.add(new MediaType(suffix, "application/pics-rules", FILE_TYPE_UNKNOWN));
                break;
            case ".prt":
                result.add(new MediaType(suffix, "application/x-prt", FILE_TYPE_UNKNOWN));
                break;
            case ".ra":
                result.add(new MediaType(suffix, "audio/vnd.rn-realaudio", FILE_TYPE_AUDIO));
                break;
            case ".ras":
                result.add(new MediaType(suffix, "application/x-ras", FILE_TYPE_UNKNOWN));
                break;
            case ".ppm":
                result.add(new MediaType(suffix, "application/x-ppm", FILE_TYPE_UNKNOWN));
                break;
            case ".pr":
                result.add(new MediaType(suffix, "application/x-pr", FILE_TYPE_UNKNOWN));
                break;
            case ".prn":
                result.add(new MediaType(suffix, "application/x-prn", FILE_TYPE_UNKNOWN));
                break;
            case ".ptn":
                result.add(new MediaType(suffix, "application/x-ptn", FILE_TYPE_UNKNOWN));
                break;
            case ".red":
                result.add(new MediaType(suffix, "application/x-red", FILE_TYPE_UNKNOWN));
                break;
            case ".rjs":
                result.add(new MediaType(suffix, "application/vnd.rn-realsystem-rjs", FILE_TYPE_UNKNOWN));
                break;
            case ".rlc":
                result.add(new MediaType(suffix, "application/x-rlc", FILE_TYPE_UNKNOWN));
                break;
            case ".rm":
                result.add(new MediaType(suffix, "application/vnd.rn-realmedia", FILE_TYPE_UNKNOWN));
                break;
            case ".rat":
                result.add(new MediaType(suffix, "application/rat-file", FILE_TYPE_UNKNOWN));
                break;
            case ".rec":
                result.add(new MediaType(suffix, "application/vnd.rn-recording", FILE_TYPE_UNKNOWN));
                break;
            case ".rgb":
                result.add(new MediaType(suffix, "application/x-rgb", FILE_TYPE_UNKNOWN));
                break;
            case ".rjt":
                result.add(new MediaType(suffix, "application/vnd.rn-realsystem-rjt", FILE_TYPE_UNKNOWN));
                break;
            case ".rle":
                result.add(new MediaType(suffix, "application/x-rle", FILE_TYPE_UNKNOWN));
                break;
            case ".rmf":
                result.add(new MediaType(suffix, "application/vnd.adobe.rmf", FILE_TYPE_UNKNOWN));
                break;
            case ".rmj":
                result.add(new MediaType(suffix, "application/vnd.rn-realsystem-rmj", FILE_TYPE_UNKNOWN));
                break;
            case ".rmp":
                result.add(new MediaType(suffix, "application/vnd.rn-rn_music_package", FILE_TYPE_UNKNOWN));
                break;
            case ".rmvb":
                result.add(new MediaType(suffix, "application/vnd.rn-realmedia-vbr", FILE_TYPE_VIDEO));
                break;
            case ".rnx":
                result.add(new MediaType(suffix, "application/vnd.rn-realplayer", FILE_TYPE_UNKNOWN));
                break;
            case ".rpm":
                result.add(new MediaType(suffix, "audio/x-pn-realaudio-plugin", FILE_TYPE_UNKNOWN));
                break;
            case ".rms":
                result.add(new MediaType(suffix, "application/vnd.rn-realmedia-secure", FILE_TYPE_UNKNOWN));
                break;
            case ".rmx":
                result.add(new MediaType(suffix, "application/vnd.rn-realsystem-rmx", FILE_TYPE_UNKNOWN));
                break;
            case ".rsml":
                result.add(new MediaType(suffix, "application/vnd.rn-rsml", FILE_TYPE_UNKNOWN));
                break;
            case ".rv":
                result.add(new MediaType(suffix, "video/vnd.rn-realvideo", FILE_TYPE_UNKNOWN));
                break;
            case ".sat":
                result.add(new MediaType(suffix, "application/x-sat", FILE_TYPE_UNKNOWN));
                break;
            case ".sdw":
                result.add(new MediaType(suffix, "application/x-sdw", FILE_TYPE_UNKNOWN));
                break;
            case ".slb":
                result.add(new MediaType(suffix, "application/x-slb", FILE_TYPE_UNKNOWN));
                break;
            case ".sam":
                result.add(new MediaType(suffix, "application/x-sam", FILE_TYPE_UNKNOWN));
                break;
            case ".sdp":
                result.add(new MediaType(suffix, "application/sdp", FILE_TYPE_UNKNOWN));
                break;
            case ".sit":
                result.add(new MediaType(suffix, "application/x-stuffit", FILE_TYPE_UNKNOWN));
                break;
            case ".sld":
                result.add(new MediaType(suffix, "application/x-sld", FILE_TYPE_UNKNOWN));
                break;
            case ".smi":
            case ".smil":
                result.add(new MediaType(suffix, "application/smil", FILE_TYPE_UNKNOWN));
                break;
            case ".smk":
                result.add(new MediaType(suffix, "application/x-smk", FILE_TYPE_UNKNOWN));
                break;
            case ".spl":
                result.add(new MediaType(suffix, "application/futuresplash", FILE_TYPE_UNKNOWN));
                break;
            case ".ssm":
                result.add(new MediaType(suffix, "application/streamingmedia", FILE_TYPE_UNKNOWN));
                break;
            case ".stl":
                result.add(new MediaType(suffix, "application/vnd.ms-pki.stl", FILE_TYPE_UNKNOWN));
                break;
            case ".sst":
                result.add(new MediaType(suffix, "application/vnd.ms-pki.certstore", FILE_TYPE_UNKNOWN));
                break;
            case ".tdf":
                result.add(new MediaType(suffix, "application/x-tdf", FILE_TYPE_UNKNOWN));
                break;
            case ".tga":
                result.add(new MediaType(suffix, "application/x-tga", FILE_TYPE_UNKNOWN));
                break;
            case ".sty":
                result.add(new MediaType(suffix, "application/x-sty", FILE_TYPE_UNKNOWN));
                break;
            case ".tg4":
                result.add(new MediaType(suffix, "application/x-tg4", FILE_TYPE_UNKNOWN));
                break;
            case ".vst":
                result.add(new MediaType(suffix, "application/x-vst", FILE_TYPE_UNKNOWN));
                result.add(new MediaType(suffix, "application/vnd.visio", FILE_TYPE_UNKNOWN));
                break;
            case ".vsw":
            case ".vdx":
            case ".vtx":
            case ".vss":
            case ".vsx ":
                result.add(new MediaType(suffix, "application/vnd.visio", FILE_TYPE_UNKNOWN));
                break;
            case ".vpg":
                result.add(new MediaType(suffix, "application/x-vpeg005", FILE_TYPE_UNKNOWN));
                break;
            case ".vsd":
                result.add(new MediaType(suffix, "application/x-vsd", FILE_TYPE_UNKNOWN));
                result.add(new MediaType(suffix, "application/vnd.visio", FILE_TYPE_UNKNOWN));
                break;
            case ".torrent":
                result.add(new MediaType(suffix, "application/x-bittorrent", FILE_TYPE_UNKNOWN));
                break;
            case ".vda":
                result.add(new MediaType(suffix, "application/x-vda", FILE_TYPE_UNKNOWN));
                break;
            case ".wb1":
                result.add(new MediaType(suffix, "application/x-wb1", FILE_TYPE_UNKNOWN));
                break;
            case ".wb3":
                result.add(new MediaType(suffix, "application/x-wb3", FILE_TYPE_UNKNOWN));
                break;
            case ".wk4":
                result.add(new MediaType(suffix, "application/x-wk4", FILE_TYPE_UNKNOWN));
                break;
            case ".wks":
                result.add(new MediaType(suffix, "application/x-wks", FILE_TYPE_UNKNOWN));
                break;
            case ".wb2":
                result.add(new MediaType(suffix, "application/x-wb2", FILE_TYPE_UNKNOWN));
                break;
            case ".wk3":
                result.add(new MediaType(suffix, "application/x-wk3", FILE_TYPE_UNKNOWN));
                break;
            case ".wkq":
                result.add(new MediaType(suffix, "application/x-wkq", FILE_TYPE_UNKNOWN));
                break;
            case ".wmf":
                result.add(new MediaType(suffix, "application/x-wmf", FILE_TYPE_UNKNOWN));
                break;
            case ".wmd":
                result.add(new MediaType(suffix, "application/x-ms-wmd", FILE_TYPE_UNKNOWN));
                break;
            case ".wp6":
                result.add(new MediaType(suffix, "application/x-wp6", FILE_TYPE_UNKNOWN));
                break;
            case ".wpg":
                result.add(new MediaType(suffix, "application/x-wpg", FILE_TYPE_UNKNOWN));
                break;
            case ".wq1":
                result.add(new MediaType(suffix, "application/x-wq1", FILE_TYPE_UNKNOWN));
                break;
            case ".wri":
                result.add(new MediaType(suffix, "application/x-wri", FILE_TYPE_UNKNOWN));
                break;
            case ".ws":
            case ".ws2":
                result.add(new MediaType(suffix, "application/x-ws", FILE_TYPE_UNKNOWN));
                break;
            case ".wmz":
                result.add(new MediaType(suffix, "application/x-ms-wmz", FILE_TYPE_UNKNOWN));
                break;
            case ".wpd":
                result.add(new MediaType(suffix, "application/x-wpd", FILE_TYPE_UNKNOWN));
                break;
            case ".wpl":
                result.add(new MediaType(suffix, "application/vnd.ms-wpl", FILE_TYPE_UNKNOWN));
                break;
            case ".wr1":
                result.add(new MediaType(suffix, "application/x-wr1", FILE_TYPE_UNKNOWN));
                break;
            case ".wrk":
                result.add(new MediaType(suffix, "application/x-wrk", FILE_TYPE_UNKNOWN));
                break;
            case ".xdp":
                result.add(new MediaType(suffix, "application/vnd.adobe.xdp", FILE_TYPE_UNKNOWN));
                break;
            case ".xfd":
                result.add(new MediaType(suffix, "application/vnd.adobe.xfd", FILE_TYPE_UNKNOWN));
                break;
            case ".xfdf":
                result.add(new MediaType(suffix, "application/vnd.adobe.xfdf", FILE_TYPE_UNKNOWN));
                break;
            case ".xwd":
                result.add(new MediaType(suffix, "application/x-xwd", FILE_TYPE_UNKNOWN));
                break;
            case ".sis":
            case ".sisx":
                result.add(new MediaType(suffix, "application/vnd.symbian.install", FILE_TYPE_UNKNOWN));
                break;
            case ".x_t":
                result.add(new MediaType(suffix, "application/x-x_t", FILE_TYPE_UNKNOWN));
                break;
            case ".x_b":
                result.add(new MediaType(suffix, "application/x-x_b", FILE_TYPE_UNKNOWN));
                break;
            case ".ipa":
                result.add(new MediaType(suffix, "application/vnd.iphone", FILE_TYPE_UNKNOWN));
                break;
            case ".xap":
                result.add(new MediaType(suffix, "application/x-silverlight-app", FILE_TYPE_UNKNOWN));
                break;
            case ".xlw":
                result.add(new MediaType(suffix, "application/x-xlw", FILE_TYPE_UNKNOWN));
                break;
            case ".anv":
                result.add(new MediaType(suffix, "application/x-anv", FILE_TYPE_UNKNOWN));
                break;
            case ".uin":
                result.add(new MediaType(suffix, "application/x-icq", FILE_TYPE_UNKNOWN));
                break;
            case ".*":
                result.add(new MediaType(suffix, "application/octet-stream", FILE_TYPE_UNKNOWN));
                break;
            case ".top":
                result.add(new MediaType(suffix, "drawing/x-top", FILE_TYPE_UNKNOWN));
                break;
            case ".slk":
                result.add(new MediaType(suffix, "drawing/x-slk", FILE_TYPE_UNKNOWN));
                break;
            case ".907":
                result.add(new MediaType(suffix, "drawing/907", FILE_TYPE_UNKNOWN));
                break;
            case ".eml":
            case ".mht":
            case ".mhtml":
            case ".nws":
                result.add(new MediaType(suffix, "message/rfc822", FILE_TYPE_UNKNOWN));
                break;
            case ".java":
            case ".class":
                result.add(new MediaType(suffix, "java/*", FILE_TYPE_UNKNOWN));
                break;
        }
        result.add(new MediaType(suffix, "*/*", FILE_TYPE_UNKNOWN));
        return result;
    }


}
