package com.zxf.utils;

import java.util.Formatter;
import java.util.Locale;

public class NumberFormat {

    public static final long B = 1;
    public static final long KB = 1024 * B;
    public static final long MB = 1024 * KB;
    public static final long GB = 1024 * MB;
    public static final long TB = 1024 * GB;

    /**
     * 存储容量转换
     *
     * @param size    单位为B
     * @param decimal 保留小数点的位数
     * @return 转换后的单位
     */
    public static String longToFileSize(long size, int decimal) {
        java.text.NumberFormat format = java.text.NumberFormat.getNumberInstance();
        decimal = (decimal < 0) ? 0 : ((decimal > 5) ? 5 : decimal);
        format.setMaximumFractionDigits(decimal);
        if (size > TB) {
            return format.format((double) size / (double) TB) + "TB";
        } else if (size > GB) {
            return format.format((double) size / (double) GB) + "GB";
        } else if (size > MB) {
            return format.format((double) size / (double) MB) + "MB";
        } else if (size > KB) {
            return format.format((double) size / (double) KB) + "KB";
        } else if (size >= B) {
            return format.format((double) size / (double) B) + "B";
        } else {
            return "--";
        }
    }


    private static final int millisecond = 1;
    private static final int second = millisecond * 1000;
    private static final int minute = 60 * second;
    private static final int hour = 60 * minute;

    /**
     * 毫秒数转小时
     *
     * @param milliseconds
     * @return
     */
    public static String longToHours(long milliseconds) {
        if (milliseconds > hour) {
            return fill((int) (milliseconds / hour), 2) + ":" + fill((int) (milliseconds % hour) / minute, 2) + ":" + fill((int) (milliseconds % minute) / second, 2);
        } else if (milliseconds > minute) {
            return fill((int) (milliseconds / minute), 2) + ":" + fill((int) (milliseconds % minute) / second, 2);
        } else {
            return "00:" + fill((int) (milliseconds / second), 2);
        }
    }


    /**
     * 在开始位置补0
     *
     * @param data   数据
     * @param length 转换后数据长度
     * @return
     */
    public static String fill(int data, int length) {
        Formatter formatter = new Formatter(Locale.CHINA);
        formatter.format("%0" + length + "d", data);
        return formatter.toString();
    }

    /**
     * 每3位数字添加一个逗号
     *
     * @param data   数据
     * @param length 转换后数据长度
     * @return
     */
    public static String separator(int data, int length) {
        Formatter formatter = new Formatter(Locale.CHINA);
        formatter.format("%," + length + "d", data);
        return formatter.toString();
    }


    /**
     * 左对齐
     *
     * @param data   数据
     * @param length 转换后数据长度
     * @return
     */
    public static String alignLeft(int data, int length) {
        Formatter formatter = new Formatter(Locale.CHINA);
        formatter.format("%-" + length + "d", data);
        return formatter.toString();
    }

    /**
     * 右对齐
     *
     * @param data   数据
     * @param length 转换后数据长度
     * @return
     */
    public static String alignRight(int data, int length) {
        Formatter formatter = new Formatter(Locale.CHINA);
        formatter.format("%" + length + "d", data);
        return formatter.toString();
    }

    /**
     * 输出带+/-符号的字符串
     *
     * @param data   数据
     * @param length 转换后数据长度
     * @return
     */
    public static String sign(int data, int length) {
        Formatter formatter = new Formatter(Locale.CHINA);
        formatter.format("%+" + length + "d", data);
        return formatter.toString();
    }


}
