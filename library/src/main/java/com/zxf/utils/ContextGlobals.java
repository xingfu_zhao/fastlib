package com.zxf.utils;

import android.app.Application;
import android.os.Handler;
import android.os.Looper;

import java.lang.reflect.Method;

public class ContextGlobals {

    private static Application application = null;

    public synchronized static Application getApplication() {
        synchronized (ContextGlobals.class) {
            if (application != null) {
                return application;
            }
            if (Looper.myLooper() == Looper.getMainLooper()) {
                application = getApplicationOnMain();
            } else {
                try {
                    final Object lock = new Object();
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            application = getApplicationOnMain();
                            lock.notify();
                        }
                    });
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return application;
        }
    }


    /**
     * 获取当前应用的Application
     * 先使用ActivityThread里获取Application的方法，如果没有获取到，
     * 再使用AppGlobals里面的获取Application的方法
     * <p>此方法必须在主线程使用</p>
     *
     * @return
     */
    private static Application getApplicationOnMain() {
        Application application = null;
        try {
            Class atClass = Class.forName("android.app.ActivityThread");
            Method currentApplicationMethod = atClass.getDeclaredMethod("currentApplication");
            currentApplicationMethod.setAccessible(true);
            application = (Application) currentApplicationMethod.invoke(null);
        } catch (Exception e) {
        }

        if (application != null)
            return application;
        try {
            Class atClass = Class.forName("android.app.AppGlobals");
            Method currentApplicationMethod = atClass.getDeclaredMethod("getInitialApplication");
            currentApplicationMethod.setAccessible(true);
            application = (Application) currentApplicationMethod.invoke(null);
        } catch (Exception e) {
        }

        return application;
    }
}
