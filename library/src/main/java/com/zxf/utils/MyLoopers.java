package com.zxf.utils;

import android.os.Build;
import android.os.HandlerThread;
import android.os.Looper;

import androidx.annotation.NonNull;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class MyLoopers {
    private final static Map<String, Looper> looperMap = new ConcurrentHashMap<>();

    public static Looper getThreadLooper() {
        return getThreadLooper("default-thread");
    }

    public synchronized static Looper getThreadLooper(@NonNull String name) {
        Looper looper = looperMap.get(name);
        if (looper == null || !looper.getThread().isAlive()) {
            HandlerThread thread = new HandlerThread(name);
            thread.start();
            looper = thread.getLooper();
            looperMap.put(name, looper);
        }
        return looper;
    }

    public synchronized static void quit(@NonNull String name) {
        Looper looper = looperMap.get(name);
        if (looper != null) {
            looperMap.remove(name);
            quit(looper);
        }
    }

    public synchronized static void quit(Looper looper) {
        if (looper != null && looper != Looper.getMainLooper()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                looper.quitSafely();
            }
            looper.quit();
            looper = null;
        }
    }

    public static Looper getMainLooper() {
        return Looper.getMainLooper();
    }

}
