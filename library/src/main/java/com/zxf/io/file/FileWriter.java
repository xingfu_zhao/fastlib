package com.zxf.io.file;


import com.zxf.io.utils.CloseUtils;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;

public class FileWriter {

    private File file;

    /**
     * 对文件执行写操作
     *
     * @param path 文件路径
     */
    public FileWriter(String path) {
        file = new File(path);
    }

    /**
     * 对文件执行写操作
     *
     * @param file 文件路径
     */
    public FileWriter(File file) {
        this.file = file;
    }

    /**
     * 将数据源的内容写入到文件
     *
     * @param text   数据源
     * @param append {@code true 追加},
     *               {@code false 覆盖}
     * @return {@code true 成功},{@code false 失败}
     */
    public boolean write(String text, boolean append) {
        BufferedOutputStream bos = null;
        try {
            bos = new BufferedOutputStream(new FileOutputStream(file, append));
            bos.write(text.getBytes());
            bos.flush();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            CloseUtils.close(bos);
        }
    }

    /**
     * 将数据源的内容写入到文件
     *
     * @param text    数据源
     * @param append  {@code true 追加},
     *                {@code false 覆盖}
     * @param charset 编码方式
     * @return {@code true 成功},{@code false 失败}
     */
    public boolean write(String text, boolean append, Charset charset) {
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, append), charset));
            writer.newLine();
            writer.write(text);
            writer.flush();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            CloseUtils.close(writer);
        }
    }

    /**
     * 将数据源的内容写入到文件
     *
     * @param bytes  数据源
     * @param append {@code true 追加},
     *               {@code false 覆盖}
     * @return {@code true 成功},{@code false 失败}
     */
    public boolean write(byte[] bytes, boolean append) {
        BufferedOutputStream bos = null;
        try {
            bos = new BufferedOutputStream(new FileOutputStream(file, append));
            bos.write(bytes);
            bos.flush();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            CloseUtils.close(bos);
        }
    }

    /**
     * 将数据源的内容写入到文件
     *
     * @param is     数据源
     * @param append {@code true 追加},
     *               {@code false 覆盖}
     * @return {@code true 成功},{@code false 失败}
     */
    public boolean write(InputStream is, boolean append) {
        BufferedOutputStream bos = null;
        try {
            bos = new BufferedOutputStream(new FileOutputStream(file, append));
            int len = -1;
            byte[] buffer = new byte[1024];
            while ((len = is.read(buffer)) != -1) {
                bos.write(buffer, 0, len);
            }
            bos.flush();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            CloseUtils.close(bos, is);
        }
    }

    public OutputStream getOutputStream() throws FileNotFoundException {
        return new BufferedOutputStream(new FileOutputStream(file));
    }
}
