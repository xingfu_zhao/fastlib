package com.zxf.io.file;

import java.io.File;
import java.text.Collator;
import java.util.Comparator;
import java.util.Locale;

/**
 * File 比较器
 */
public class FileComparators {

    /**
     * 按时间排序
     *
     * @param isAsc {@code true} 升序；{@code false} 降序
     * @return
     */
    public static Comparator<File> sortByTime(boolean isAsc) {
        return new FileDateComparator(isAsc);
    }

    /**
     * 按名称排序
     *
     * @param isAsc {@code true} 升序；{@code false} 降序
     * @return
     */
    public static Comparator<File> sortByName(boolean isAsc) {
        return new FileNameComparator(isAsc);
    }

    /**
     * 按文件大小排序
     *
     * @param isAsc {@code true} 升序；{@code false} 降序
     * @return
     */
    public static Comparator<File> sortBySize(boolean isAsc) {
        return new FileSizeComparator(isAsc);
    }

    /**
     * 比较文件名称
     */
    private static class FileNameComparator implements Comparator<File> {
        private boolean isAsc = false;

        public FileNameComparator(boolean isAsc) {
            this.isAsc = isAsc;
        }

        @Override
        public int compare(File o1, File o2) {
            if (isAsc) {
                return Collator.getInstance(Locale.CHINESE).compare(o1.getName().toLowerCase(), o2.getName().toLowerCase());
            } else {
                return Collator.getInstance(Locale.CHINESE).compare(o2.getName().toLowerCase(), o1.getName().toLowerCase());
            }
        }
    }

    /**
     * 比较文件大小
     */
    private static class FileSizeComparator implements Comparator<File> {
        private boolean isAsc = false;

        public FileSizeComparator(boolean isAsc) {
            this.isAsc = isAsc;
        }

        @Override
        public int compare(File o1, File o2) {
            if (isAsc) {
                return o1.length() > o2.length() ? 1 : -1;
            } else {
                return o1.length() < o2.length() ? 1 : -1;
            }
        }
    }

    /**
     * 比较文件最后一次修改时间
     */
    private static class FileDateComparator implements Comparator<File> {
        private boolean isAsc = false;

        public FileDateComparator(boolean isAsc) {
            this.isAsc = isAsc;
        }

        @Override
        public int compare(File o1, File o2) {
            if (isAsc) {
                return o1.lastModified() < o2.lastModified() ? 1 : -1;
            } else {
                return o1.lastModified() > o2.lastModified() ? 1 : -1;
            }
        }
    }
}
