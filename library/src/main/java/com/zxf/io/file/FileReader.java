package com.zxf.io.file;


import com.zxf.io.utils.CloseUtils;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class FileReader {

    private File file;

    /**
     * 对文件执行读操作
     *
     * @param path 文件路径
     */
    public FileReader(String path) {
        this.file = new File(path);
    }

    /**
     * 对文件执行读操作
     *
     * @param file 文件路径
     */
    public FileReader(File file) {
        this.file = file;
    }

    /**
     * 将数据源转换成文本形式
     *
     * @return
     */
    public String readText() {
        ByteArrayOutputStream baos = null;
        BufferedInputStream bis = null;
        try {
            bis = new BufferedInputStream(new FileInputStream(file));
            baos = new ByteArrayOutputStream();
            int len = -1;
            byte[] buffer = new byte[1024];
            while ((len = bis.read(buffer)) != -1) {
                baos.write(buffer, 0, len);
            }
            return baos.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            CloseUtils.close(bis, baos);
        }
        return null;
    }

    /**
     * 将数据源转换成文本形式
     *
     * @return
     */
    public String readText(Charset charset) {
        ByteArrayOutputStream baos = null;
        BufferedInputStream bis = null;
        try {
            bis = new BufferedInputStream(new FileInputStream(file));
            baos = new ByteArrayOutputStream();
            int len = -1;
            byte[] buffer = new byte[1024];
            while ((len = bis.read(buffer)) != -1) {
                baos.write(buffer, 0, len);
            }
            return baos.toString(charset.name());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            CloseUtils.close(bis, baos);
        }
        return null;
    }

    /**
     * 将数据源转换成byte[]
     *
     * @return
     */
    public byte[] readBytes() {
        ByteArrayOutputStream baos = null;
        BufferedInputStream bis = null;
        try {
            bis = new BufferedInputStream(new FileInputStream(file));
            baos = new ByteArrayOutputStream();
            int len = -1;
            byte[] buffer = new byte[1024];
            while ((len = bis.read(buffer)) != -1) {
                baos.write(buffer, 0, len);
            }
            return baos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            CloseUtils.close(bis, baos);
        }
        return null;
    }

    /**
     * 将数据源转换成InputStream
     *
     * @return
     */
    public InputStream getInputStream() throws FileNotFoundException {
        return new BufferedInputStream(new FileInputStream(file));
    }


}
