package com.zxf.io.file;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.zxf.utils.MediaType;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class FileUtils {
    public static final long KB = 1024;
    public static final long MB = 1024 * KB;
    public static final long GB = 1024 * MB;
    public static final long TB = 1024 * GB;


    public static String getFileSizeName(double size) {
        DecimalFormat format = new DecimalFormat("#.##");
        if (size > TB) {
            return format.format(size / TB) + " TB";
        } else if (size > GB) {
            return format.format(size / GB) + " GB";
        } else if (size > MB) {
            return format.format(size / MB) + " MB";
        } else if (size > KB) {
            return format.format(size / KB) + " KB";
        } else {
            return format.format(size) + " B";
        }
    }


    /**
     * 是否是同一个文件
     *
     * @param path1
     * @param path2
     * @return
     */
    public static boolean isSameFile(String path1, String path2) {
        return new File(path1).equals(new File(path2));
//        if (path1 == null || path2 == null) {
//            return false;
//        }
//        if (path1.equals(path2)) {
//            return true;
//        }
//        File file1 = new File(path1);
//        File file2 = new File(path2);
//        try {
//            String _path1 = file1.getCanonicalFile().getAbsolutePath();
//            String _path2 = file2.getCanonicalFile().getAbsolutePath();
//            if (_path1.equals(_path2)) {
//                return true;
//            }
//            if (!_path1.startsWith("/")) {
//                _path1 = "/" + _path1;
//            }
//            if (!_path2.startsWith("/")) {
//                _path2 = "/" + _path2;
//            }
//
//            if (!_path1.endsWith("/")) {
//                _path1 = _path1 + "/";
//            }
//            if (!_path2.endsWith("/")) {
//                _path2 = _path2 + "/";
//            }
//            return _path1.equals(_path2);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return false;
    }

    /**
     * file是否存在于dirs，或file是dirs中某个文件夹下的子文件/子目录
     * <p>
     * 所有参数必须已经使用{@link File#getCanonicalFile()}以避免符号链接或路径遍历攻击。
     *
     * @param file 目标文件
     * @param dirs 参考文件集合
     * @return
     */
    public static boolean contains( File file,  File... dirs) {
        int num = dirs == null ? 0 : dirs.length;
        for (int i = 0; i < num; i++) {
            String dirPath = dirs[i].getAbsolutePath();
            String filePath = file.getAbsolutePath();
            if (dirPath.equals(filePath)) {
                return true;
            }
            if (!dirPath.endsWith("/")) {
                dirPath += "/";
            }
            if (filePath.startsWith(dirPath)) {
                return true;
            }
        }
        return false;
    }


    /**
     * 打卡系统文件管理器选择文件
     *
     * @param activity
     * @param fileType    {@link MediaType#fileType}
     * @param requestCode
     * @return
     */
    public static boolean chooseFile(Activity activity, String fileType, int requestCode) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        if (fileType == null || fileType.length() <= 0
                || MediaType.FILE_TYPE_UNKNOWN.equalsIgnoreCase(fileType)) {
            intent.setType("*/*");
        } else {
            intent.setType(fileType + "/*");
        }
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        try {
            activity.startActivityForResult(Intent.createChooser(intent, "请选择文件"), requestCode);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    /**
     * 打开文件
     */
    public static boolean openFile(Context context, File file) {
        String path = file.getPath();
        List<MediaType> mediaTypes = MediaType.fromSuffix(path.substring(path.lastIndexOf(".")));
        for (MediaType mediaType : mediaTypes) {
            if (openFile(context, file, mediaType.mimeType)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 打开文件
     */
    public static boolean openFile(Context context, File file, String mimeType) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.fromFile(file), mimeType);//设置intent的data和Type属性
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(intent);            //跳转
            return true;
        } catch (Exception e) { //当系统没有携带文件打开软件，提示
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 通知系统更新文件
     *
     * @param context
     * @param file
     */
    private static void notifyFile(Context context, File file) {
        Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        intent.setData(Uri.fromFile(file));// 需要更新的文件路径
        context.sendBroadcast(intent);
    }


    /**
     * 遍历文件夹下所有文件(包含文件夹)
     *
     * @param filePath        文件路径
     * @param listChildFolder 是否遍历子文件夹下的文件
     * @return
     */
    public static List<File> listFolder(String filePath, boolean listChildFolder) {
        return listFolder(new File(filePath), listChildFolder);
    }

    /**
     * 遍历文件夹下所有文件(包含文件夹)
     *
     * @param dir             文件路径
     * @param listChildFolder 是否遍历子文件夹下的文件
     * @return
     */
    public static List<File> listFolder(File dir, boolean listChildFolder) {
        List<File> list = new ArrayList<>();
        if (dir == null || !dir.exists() || dir.isFile()) {
            return list;
        }
        File[] files = dir.listFiles();
        if (files != null) {
            list.addAll(Arrays.asList(files));
            if (listChildFolder) {
                for (File file : files) {
                    if (file.isDirectory()) {
                        list.addAll(listFolder(file, listChildFolder));
                    }
                }
            }
        }
        return list;
    }

    /**
     * 遍历文件夹下指定类型的文件
     *
     * @param filePath        文件路径
     * @param suffix          文件后缀
     * @param listChildFolder 是否遍历子文件夹下的文件
     * @return
     */
    public static List<File> listFiles(String filePath, String suffix, boolean listChildFolder) {
        return listFiles(new File(filePath), suffix, listChildFolder);
    }

    /**
     * 遍历文件夹下指定类型的文件
     *
     * @param dir             文件路径
     * @param suffix          文件后缀
     * @param listChildFolder 是否遍历子文件夹下的文件
     * @return
     */
    public static List<File> listFiles(File dir, String suffix, boolean listChildFolder) {
        List<File> list = new ArrayList<>();
        if (dir == null || !dir.exists() || dir.isFile()) {
            return list;
        }
        File[] files = dir.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isFile()) {
                    if (suffix == null) {
                        list.add(file);
                    } else if (file.getName().endsWith(suffix)) {
                        list.add(file);
                    }
                } else if (listChildFolder) {
                    list.addAll(listFiles(file, suffix, listChildFolder));
                }
            }
        }
        return list;
    }

    /**
     * 遍历删除整个文件夹
     *
     * @param filePath      文件路径
     * @param deleteRootDir 是否删除根目录
     * @return 删除文件的个数
     */
    public static int delete(String filePath, boolean deleteRootDir) {
        return delete(new File(filePath), deleteRootDir);
    }

    /**
     * 遍历删除文件或文件夹
     *
     * @param dir           文件路径
     * @param deleteRootDir 是否删除根目录
     * @return 删除文件的个数
     */
    public static int delete(File dir, boolean deleteRootDir) {
        int num = 0;
        if (dir == null || !dir.exists()) {
            return num;
        }
        if (dir.isFile()) {
            dir.delete();
            num += 1;
            return num;
        }
        File[] files = dir.listFiles();
        for (File file : files) {
            if (file.isFile()) {
                num += 1;
                file.delete();
            } else {
                num += delete(file, true);
            }
        }
        if (deleteRootDir) {
            dir.delete();
        }
        return num;
    }

    /**
     * 遍历文件夹下文件大小，若超出最大容量则删除最早的文件
     *
     * @param path        文件夹
     * @param maxSize     允许的最大容量
     * @param childFolder 是否包含子文件夹
     * @return 删除文件的大小
     */
    public static long deleteEarlyFiles(String path, long maxSize, boolean childFolder) {
        return deleteEarlyFiles(new File(path), maxSize, childFolder);
    }

    /**
     * 遍历文件夹下文件大小，若超出最大容量则删除最早的文件
     *
     * @param dir         文件夹
     * @param maxSize     允许的最大容量
     * @param childFolder 是否包含子文件夹
     * @return 删除文件的大小
     */
    public static long deleteEarlyFiles(File dir, long maxSize, boolean childFolder) {
        if (dir == null || !dir.exists() || dir.isFile()) {
            return 0;
        }
        List<File> files = listFolder(dir, childFolder);
        int num = files == null ? 0 : files.size();
        if (num <= 0) {
            return 0;
        }
        Collections.sort(files, FileComparators.sortByTime(false));
        long dirSize = getFolderSize(dir, childFolder);
        long deleteSize = 0;
        for (int i = 0; i < num; i++) {
            if (dirSize - deleteSize < maxSize) {
                break;
            }
            File file = files.get(i);
            if (file.isFile()) {
                deleteSize += file.length();
                file.delete();
            }
        }
        return deleteSize;
    }

    /**
     * 获取文件夹大小（单位：B）
     *
     * @param dir
     * @param childFolder 是否包含子文件夹
     * @return
     */
    public static long getFolderSize(File dir, boolean childFolder) {
        long size = 0;
        if (dir == null || !dir.exists()) {
            return 0;
        }
        if (dir.isFile()) {
            return dir.length();
        }
        File[] files = dir.listFiles();
        int num = files == null ? 0 : files.length;
        for (int i = 0; i < num; i++) {
            if (files[i].isFile()) {
                size += files[i].length();
            } else if (childFolder) {
                size += getFolderSize(dir, true);
            }
        }
        return size;
    }


    /**
     * 对文件执行写操作
     *
     * @param path 文件路径
     * @return
     */
    public static FileWriter writerTo( String path) {
        return new FileWriter(path);
    }

    /**
     * 对文件执行写操作
     *
     * @param file 文件路径
     * @return
     */
    public static FileWriter writerTo( File file) {
        return new FileWriter(file);
    }

    /**
     * 对文件执行读操作
     *
     * @param path 文件路径
     * @return
     */
    public static FileReader readFrom( String path) {
        return new FileReader(path);
    }

    /**
     * 对文件执行读操作
     *
     * @param file 文件路径
     * @return
     */
    public static FileReader readFrom( File file) {
        return new FileReader(file);
    }

    /**
     * 复制文件内容到另一个文件
     *
     * @param srcFile 源文件
     * @param desFile 目标文件
     * @return
     */
    public static boolean cpoyToFile( File srcFile,  File desFile) {
        if (!srcFile.exists() || srcFile.isDirectory()) {
            return false;
        }
        FileReader reader = new FileReader(srcFile);
        FileWriter writer = new FileWriter(desFile);
        try {
            return writer.write(reader.getInputStream(), false);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 复制文件或文件夹到目标文件夹中
     *
     * @param srcFile   源文件/文件夹
     * @param desFolder 目标文件夹
     * @return
     */
    public static boolean copyToFolder( File srcFile,  File desFolder) {
        if (srcFile == null || desFolder == null || !srcFile.exists()) {
            return false;
        }
        if (!desFolder.exists() || desFolder.isFile()) {
            desFolder.mkdirs();
        }
        File desFile = new File(desFolder, srcFile.getName());
        if (srcFile.isFile()) {
            return cpoyToFile(srcFile, desFile);
        }
        File[] files = srcFile.listFiles();
        int num = files == null ? 0 : files.length;
        for (int i = 0; i < num; i++) {
            if (!copyToFolder(files[i], desFile)) {
                return false;
            }
        }
        return true;
    }

    /**
     * 对文件列表排序
     *
     * @param files      文件列表
     * @param comparator 文件比较器
     */
    public static void sort(List<File> files, Comparator<File> comparator) {
        Collections.sort(files, comparator);
    }

}
