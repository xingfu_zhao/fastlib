package com.zxf.io.utils;

import android.annotation.TargetApi;
import android.os.Build;

import java.io.Closeable;
import java.io.IOException;

public class CloseUtils {


    public static void close(Closeable... closeables) {
        if (closeables == null) {
            return;
        }
        for (Closeable closeable : closeables) {
            if (closeable != null) {
                try {
                    closeable.close();
                    closeable = null;
                } catch (IOException e) {
                }
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static void close(AutoCloseable... closeables) {
        if (closeables == null) {
            return;
        }
        for (AutoCloseable closeable : closeables) {
            if (closeable != null) {
                try {
                    closeable.close();
                    closeable = null;
                } catch (Exception e) {
                }
            }
        }
    }
}
