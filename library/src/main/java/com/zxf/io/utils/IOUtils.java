package com.zxf.io.utils;

import android.annotation.TargetApi;
import android.os.Build;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class IOUtils {

    public static boolean copyStream(InputStream is, OutputStream os, boolean autoClose) throws IOException {
        if (is == null || os == null) {
            throw new IOException("InputStream and OutputStream must not be empty");
        }
        try {
            byte[] buffer = new byte[1024];
            int len = 0;
            while ((len = is.read(buffer)) != -1) {
                os.write(buffer, 0, len);
            }
            os.flush();
            return true;
        } catch (IOException e) {
            throw e;
        } finally {
            if (autoClose)
                close(is, os);
        }
    }

    public static void close(Closeable... closeables) {
        if (closeables == null) {
            return;
        }
        for (Closeable closeable : closeables) {
            if (closeable != null) {
                try {
                    closeable.close();
                    closeable = null;
                } catch (IOException e) {
                }
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static void close(AutoCloseable... closeables) {
        if (closeables == null) {
            return;
        }
        for (AutoCloseable closeable : closeables) {
            if (closeable != null) {
                try {
                    closeable.close();
                    closeable = null;
                } catch (Exception e) {
                }
            }
        }
    }
}
