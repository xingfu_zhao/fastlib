package com.zxf.system;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.provider.Settings;

import androidx.annotation.RequiresPermission;

public class SystemSettings {

    private Context context;
    private ContentResolver mContentResolver;
    private int defaultMode = Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC;

    public SystemSettings(Context context) {
        this.context = context;
        mContentResolver = context.getContentResolver();
        try {
            defaultMode = Settings.System.getInt(mContentResolver, Settings.System.SCREEN_BRIGHTNESS_MODE);
            if (defaultMode == Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC) {
                Settings.System.putInt(mContentResolver, Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);
            }
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
    }


    /**
     * 获取屏幕亮度
     *
     * @return
     */
    public int getSystemBrightness() {
        return Settings.System.getInt(mContentResolver, Settings.System.SCREEN_BRIGHTNESS, 125);
    }

    /**
     * 设置屏幕亮度
     *
     * @param value
     */
    @RequiresPermission(Manifest.permission.WRITE_SETTINGS)
    public void setSystemBrightness(int value) {
        Settings.System.putInt(mContentResolver, Settings.System.SCREEN_BRIGHTNESS, value);
    }

    public void release() {
        if (defaultMode == Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC) {
            Settings.System.putInt(mContentResolver, Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC);
        }
        mContentResolver = null;
        context = null;
    }

}
