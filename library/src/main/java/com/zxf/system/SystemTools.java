package com.zxf.system;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.RequiresPermission;


import java.lang.reflect.Method;

public class SystemTools {

    /**
     * 隐藏状态栏
     *
     * @param activity
     */
    public static void hideStatusBar(Activity activity) {
        setSystemUiVisibility(activity, View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }


    /**
     * 设置状态栏颜色
     *
     * @param activity
     * @param color
     */
    public static void setStatusBarColor(Activity activity, int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activity.getWindow().setStatusBarColor(color);
        }
    }

    /**
     * @param activity
     * @param flag{    View.SYSTEM_UI_FLAG_VISIBLE：显示状态栏，Activity不全屏显示(恢复到有状态的正常情况)。
     *                 View.INVISIBLE：隐藏状态栏，同时Activity会伸展全屏显示。
     *                 View.SYSTEM_UI_FLAG_FULLSCREEN：Activity全屏显示，且状态栏被隐藏覆盖掉。
     *                 View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN：Activity全屏显示，但状态栏不会被隐藏覆盖，状态栏依然可见，Activity顶端布局部分会被状态遮住。
     *                 View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION：效果同View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
     *                 View.SYSTEM_UI_LAYOUT_FLAGS：效果同View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
     *                 View.SYSTEM_UI_FLAG_HIDE_NAVIGATION：隐藏虚拟按键(导航栏)。有些手机会用虚拟按键来代替物理按键。
     *                 View.SYSTEM_UI_FLAG_LOW_PROFILE：状态栏显示处于低能显示状态(low profile模式)，状态栏上一些图标显示会被隐藏。}
     */
    public static void setSystemUiVisibility(Activity activity, int flag) {
        View decorView = activity.getWindow().getDecorView();
        decorView.setSystemUiVisibility(flag);
    }


    /**
     * 设置系统亮度
     *
     * @param context
     * @param brightness
     */
    @RequiresPermission(Manifest.permission.WRITE_SETTINGS)
    public static void setSystemBrightness(Context context, int brightness) {
        if (brightness < 0) {
            brightness = 0;
        } else if (brightness > 255) {
            brightness = 255;
        }
        SystemSettings settings = new SystemSettings(context);
        settings.setSystemBrightness(brightness);
        settings.release();
    }

    /**
     * 获取系统亮度
     *
     * @param context
     * @return
     */
    public static int getSystemBrightness(Context context) {
        SystemSettings settings = new SystemSettings(context);
        try {
            return settings.getSystemBrightness();
        } finally {
            settings.release();
        }
    }

    /**
     * 获取当前窗口亮度
     *
     * @param activity
     * @return
     */
    public static int getWindowBrightness(Activity activity) {
        int light = (int) (activity.getWindow().getAttributes().screenBrightness * 255.0f);
        return light < 0 ? getSystemBrightness(activity) : light;
    }

    /**
     * 设置当前窗口亮度
     *
     * @param brightness
     */
    public static void setWindowBrightness(Activity activity, int brightness) {
        if (brightness < 0) {
            brightness = 0;
        } else if (brightness > 255) {
            brightness = 255;
        }
        Window window = activity.getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.screenBrightness = brightness / 255.0f;
        window.setAttributes(lp);
    }

    /**
     * 获取屏幕高宽
     *
     * @param context
     * @return
     */
    public static Point getScreenSize(Context context) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        return new Point(dm.widthPixels, dm.heightPixels);
    }

    /**
     * 获取状态栏高度
     *
     * @return
     */
    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    /**
     * 获取标题栏高度
     *
     * @return
     */
    public static int getActionBarHeight(Context context) {
        TypedValue tv = new TypedValue();
        if (context.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            return TypedValue.complexToDimensionPixelSize(tv.data, context.getResources().getDisplayMetrics());
        }
        return 0;
    }


    /**
     * 禁止下拉状态栏
     *
     * @param context
     */
    @RequiresPermission(allOf = {Manifest.permission.EXPAND_STATUS_BAR, Manifest.permission.STATUS_BAR})
    public static void disableDropdownStatusBar(Context context) {
        try {
            @SuppressLint("WrongConstant")
            Object mStatusBarManager = context.getSystemService("statusbar");
            Class<?> mStatusBarManagerClazz = Class.forName("android.app.StatusBarManager");
            Method expand = mStatusBarManagerClazz.getMethod("disable", int.class);
            //判断版本大小
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                expand.invoke(mStatusBarManager, 0x00010000);
            } else {
                expand.invoke(mStatusBarManager, 0x00000001);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 允许下拉状态栏
     *
     * @param context
     */
    @RequiresPermission(allOf = {Manifest.permission.EXPAND_STATUS_BAR, Manifest.permission.STATUS_BAR})
    public static void enableDropdownStatusBar(Context context) {
        try {
            @SuppressLint("WrongConstant")
            Object mStatusBarManager = context.getSystemService("statusbar");
            Class<?> mStatusBarManagerClazz = Class
                    .forName("android.app.StatusBarManager");
            Method expand = mStatusBarManagerClazz.getMethod("disable", int.class);
            expand.invoke(mStatusBarManager, 0x00000000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 获取导航栏高度
     *
     * @param context
     * @return
     */
    public static int getNavigationBarHeight(Context context) {
        Resources resources = context.getResources();
        int resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android");
        int height = resources.getDimensionPixelSize(resourceId);
        return height;
    }
}
