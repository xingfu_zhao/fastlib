package com.zxf.toolbar;

import android.content.res.ColorStateList;
import android.os.Build;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.appbar.AppBarLayout;
import com.zxf.fastlibrary.R;


public class ToolbarUtils {

    public static ToolbarUtils bind(AppCompatActivity activity, @StringRes int title) {
        ToolbarUtils toolbarUtils = new ToolbarUtils(activity);
        toolbarUtils.setTitle(title);
        toolbarUtils.setBackEnabled(true);
        return toolbarUtils;
    }

    public static ToolbarUtils bind(AppCompatActivity activity, String title) {
        ToolbarUtils toolbarUtils = new ToolbarUtils(activity);
        toolbarUtils.setTitle(title);
        toolbarUtils.setBackEnabled(true);
        return toolbarUtils;
    }


    private TextView titleView;
    private ImageView backView;
    private AppBarLayout mAppBarLayout;

    private ToolbarUtils(AppCompatActivity activity) {
        mAppBarLayout = activity.findViewById(R.id.appbar_layout);
        Toolbar toolbar = activity.findViewById(R.id.toolbar);
        titleView = activity.findViewById(R.id.title);
        backView = activity.findViewById(R.id.title_back);
        activity.setSupportActionBar(toolbar);
        backView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = activity.getSupportFragmentManager().getBackStackEntryCount();
                if (count > 0) {
                    activity.getSupportFragmentManager().popBackStack();
                } else {
                    activity.finish();
                }
            }
        });
    }

    private ToolbarUtils setTitle(@StringRes int stringId) {
        titleView.setText(stringId);
        return this;
    }

    private ToolbarUtils setTitle(String title) {
        titleView.setText(title);
        return this;
    }

    public ToolbarUtils setToolbarColor(int color) {
        mAppBarLayout.setBackgroundColor(color);
        return this;
    }

    public ToolbarUtils setContentColor(int color) {
        titleView.setTextColor(color);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            backView.setImageTintList(ColorStateList.valueOf(color));
        }
        return this;
    }

    public ToolbarUtils setBackEnabled(boolean enable) {
        backView.setVisibility(enable ? View.VISIBLE : View.INVISIBLE);
        return this;
    }

}
