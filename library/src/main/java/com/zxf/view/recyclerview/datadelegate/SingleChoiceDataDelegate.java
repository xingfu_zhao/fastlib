package com.zxf.view.recyclerview.datadelegate;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * 数据单选
 *
 * @param <DATA>
 */
public class SingleChoiceDataDelegate<DATA> extends SimpleDataDelegate<DATA> {

    protected final List<Boolean> checkeds = new ArrayList<>();

    public SingleChoiceDataDelegate() {
        super();
    }

    public SingleChoiceDataDelegate(Comparator<DATA> comparator) {
        super(comparator);
    }

    /**
     * 指定项是否选中
     *
     * @param position
     * @return
     */
    public boolean isChecked(int position) {
        return checkeds.get(position);
    }

    /**
     * 修改选中状态
     *
     * @param position
     */
    public void alterChecked(int position) {
        int index = checkeds.indexOf(true);
        if (index >= 0) {
            checkeds.set(index, false);
            notifyItemChanged(index);
        }
        checkeds.set(position, true);
        notifyItemChanged(position);
    }


    @Override
    public synchronized final void setDatas(List<DATA> newdatas) {
        checkeds.clear();
        if (newdatas != null && newdatas.size() > 0) {
            Boolean[] booleans = new Boolean[newdatas.size()];
            Arrays.fill(booleans, false);
            checkeds.addAll(Arrays.asList(booleans));
        }
        super.setDatas(newdatas);
    }

    @Override
    public synchronized final void add(DATA data) {
        if (data != null)
            checkeds.add(false);
        super.add(data);
    }

    @Override
    public synchronized final void add(int position, DATA data) {
        if (data != null) {
            checkeds.add(position, false);
        }
        super.add(position, data);
    }

    @Override
    public synchronized final void addAll(Collection<DATA> datas) {
        if (datas != null && datas.size() > 0) {
            Boolean[] booleans = new Boolean[datas.size()];
            Arrays.fill(booleans, false);
            checkeds.addAll(Arrays.asList(booleans));
        }
        super.addAll(datas);
    }

    @Override
    public synchronized final void addAll(int position, Collection<DATA> datas) {
        if (datas != null && datas.size() > 0) {
            Boolean[] booleans = new Boolean[datas.size()];
            Arrays.fill(booleans, false);
            checkeds.addAll(position, Arrays.asList(booleans));
        }
        super.addAll(position, datas);
    }

    @Override
    public synchronized final void replace(int position, DATA data) {
        checkeds.set(position, false);
        super.replace(position, data);
    }

    @Override
    public synchronized final void replace(DATA oldData, DATA newData) {
        int index = indexOfValue(oldData);
        checkeds.set(index, false);
        super.replace(oldData, newData);
    }

    @Override
    public synchronized final void remove(int position) {
        checkeds.remove(position);
        super.remove(position);
    }

    @Override
    public synchronized final void clear() {
        checkeds.clear();
        super.clear();
    }

    @Override
    public synchronized final void moved(int from, int to) {
        Collections.swap(checkeds, from, to);
        super.moved(from, to);
    }

    @Override
    public synchronized final void reverse() {
        Collections.reverse(checkeds);
        super.reverse();
    }
}
