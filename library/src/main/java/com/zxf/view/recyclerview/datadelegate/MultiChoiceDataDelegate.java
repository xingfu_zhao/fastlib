package com.zxf.view.recyclerview.datadelegate;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 数据多选
 *
 * @param <DATA>
 */
public class MultiChoiceDataDelegate<DATA> extends SingleChoiceDataDelegate<DATA> {

    public MultiChoiceDataDelegate() {
        super();
    }

    public MultiChoiceDataDelegate(Comparator<DATA> comparator) {
        super(comparator);
    }

    /**
     * 某项是否被选中
     *
     * @param position
     * @return
     */
    public boolean isChecked(int position) {
        return checkeds.get(position);
    }

    /**
     * 修改选中状态
     *
     * @param position
     */
    public void alterChecked(int position) {
        if (checkeds.get(position)) {
            checkeds.set(position, false);
        } else {
            checkeds.set(position, true);
        }
        notifyItemChanged(position);
    }


    /**
     * 是否被全选
     *
     * @return
     */
    public boolean isCheckedAll() {
        return !checkeds.contains(false);
    }

    /**
     * 全选
     */
    public void checkedAll() {
        Collections.fill(checkeds, true);
        notifyDataSetChanged();
    }

    /**
     * 全不选
     */
    public void uncheckedAll() {
        Collections.fill(checkeds, false);
        notifyDataSetChanged();
    }

    /**
     * 获取被选中的项
     *
     * @return
     */
    public List<DATA> getCheckedDatas() {
        List<DATA> result = new ArrayList<>();
        for (int i = 0; i < checkeds.size(); i++) {
            if (checkeds.get(i)) {
                result.add(datas.get(i));
            }
        }
        return result;
    }


}
