package com.zxf.view.recyclerview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zxf.view.recyclerview.datadelegate.SimpleDataDelegate;

import java.util.ArrayList;
import java.util.List;


/**
 * 适用于RecyclerView的适配器
 *
 * @param
 */
public abstract class RecyclerAdapter extends RecyclerView.Adapter<ViewHolder> {
    private SimpleDataDelegate itemDataDelegate;
    private int layoutId;

    public RecyclerAdapter(@LayoutRes int layoutId, @NonNull SimpleDataDelegate itemDataDelegate) {
        this.layoutId = layoutId;
        this.setDataDelegate(itemDataDelegate);
    }

    private final void setDataDelegate(SimpleDataDelegate itemDataDelegate) {
        this.itemDataDelegate = itemDataDelegate;
        this.itemDataDelegate.setAdapter(this);
    }


    @NonNull
    @Override
    public final ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
        return new ViewHolder(view);
    }

    protected abstract void onBindView(ViewHolder holder, int position);

    @Override
    public final void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final int pos = position;
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(v, pos);
                }
            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (mOnItemLongClickListener != null) {
                    return mOnItemLongClickListener.onItemLongClick(v, pos);
                }
                return false;
            }
        });
        if (clickViews.size() > 0) {
            for (Integer integer : clickViews) {
                holder.getView(integer).setOnClickListener(new ChildClick(mOnItemChildClickListener, pos));
            }
        }
        onBindView(holder, pos);
    }


    @Override
    public int getItemViewType(int position) {
        return 0;
    }


    @Override
    public int getItemCount() {
        return itemDataDelegate == null ? 0 : itemDataDelegate.size();
    }


    private OnItemLongClickListener mOnItemLongClickListener;
    private OnItemClickListener mOnItemClickListener;
    private OnItemChildClickListener mOnItemChildClickListener;

    private List<Integer> clickViews = new ArrayList<>();

    /**
     * 为item的某个子控件添加点击事件
     *
     * @param viewId
     */
    public void addItemChildClick(int viewId) {
        clickViews.add(viewId);
    }


    /**
     * 设置item子控件点击事件监听
     *
     * @param mOnItemChildClickListener
     */
    public void setOnItemChildClickListener(OnItemChildClickListener mOnItemChildClickListener) {
        this.mOnItemChildClickListener = mOnItemChildClickListener;
    }


    /**
     * 设置item长按事件监听
     *
     * @param mOnItemLongClickListener
     */
    public void setOnItemLongClickListener(OnItemLongClickListener mOnItemLongClickListener) {
        this.mOnItemLongClickListener = mOnItemLongClickListener;
    }

    /**
     * 设置item点击事件监听
     *
     * @param mOnItemClickListener
     */
    public void setOnItemClickListener(OnItemClickListener mOnItemClickListener) {
        this.mOnItemClickListener = mOnItemClickListener;
    }


    public interface OnItemLongClickListener {
        /**
         * item长按事件
         *
         * @param view
         * @param position
         * @return
         */
        boolean onItemLongClick(View view, int position);
    }


    public interface OnItemClickListener {
        /**
         * item点击事件
         */
        void onItemClick(View view, int position);
    }


    /**
     * item子控件点击事件监听
     */
    public interface OnItemChildClickListener {

        /**
         * item子控件点击事件
         *
         * @param view
         * @param position
         */
        void onItemChildClick(View view, int position);
    }


    private static class ChildClick implements View.OnClickListener {
        private OnItemChildClickListener listener;
        private int position;

        public ChildClick(OnItemChildClickListener listener, int position) {
            this.listener = listener;
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            if (listener != null) {
                listener.onItemChildClick(v, position);
            }
        }
    }

}
