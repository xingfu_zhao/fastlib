package com.zxf.view.recyclerview.datadelegate;


import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;


/**
 * 数据管理
 *
 * @param <DATA>
 */
public class SimpleDataDelegate<DATA> {

    protected List<DATA> datas = new ArrayList<>();
    private Comparator<DATA> comparator;

    protected RecyclerView.Adapter mRecycleAdapter;

    public SimpleDataDelegate() {
    }

    public SimpleDataDelegate(Comparator<DATA> comparator) {
        this.comparator = comparator;
    }


    public void setComparator(Comparator<DATA> comparator) {
        this.comparator = comparator;
    }

    public final void setAdapter(RecyclerView.Adapter mRecycleAdapter) {
        this.mRecycleAdapter = mRecycleAdapter;
    }


    /**
     * 重置数据
     *
     * @param newdatas
     * @return
     */
    public void setDatas(List<DATA> newdatas) {
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new DiffCallback<DATA>(newdatas, this.datas, comparator));
        this.datas.clear();
        if (newdatas != null && newdatas.size() > 0) {
            this.datas = new ArrayList<>(newdatas);
        }
        diffResult.dispatchUpdatesTo(mRecycleAdapter);
    }

    /**
     * 添加数据
     */
    public synchronized void add(DATA data) {
        if (data != null) {
            int start = datas.size();
            datas.add(data);
            notifyItemInserted(start);
        }
    }

    /**
     * 添加数据
     *
     * @param position
     * @param data
     */
    public synchronized void add(int position, DATA data) {
        if (data != null) {
            position = position < datas.size() ? position : datas.size();
            datas.add(position, data);
            notifyItemInserted(position);
        }
    }

    /**
     * 添加数据
     *
     * @param datas
     */
    public synchronized void addAll(Collection<DATA> datas) {
        int len = datas == null ? 0 : datas.size();
        if (len > 0) {
            int start = this.datas.size();
            this.datas.addAll(datas);
            notifyItemRangeInserted(start, len);
        }
    }

    /**
     * 添加数据
     *
     * @param position
     * @param datas
     */
    public synchronized void addAll(int position, Collection<DATA> datas) {
        int len = datas == null ? 0 : datas.size();
        if (len > 0) {
            position = position < this.datas.size() ? position : this.datas.size();
            this.datas.addAll(position, datas);
            notifyItemRangeInserted(position, len);
        }
    }

    /**
     * 更新一条数据，若数据存在则插入新数据
     *
     * @param oldData
     * @param newData
     */
    public final void update(DATA oldData, DATA newData) {
        int position = indexOfValue(oldData);
        if (position >= 0) {
            datas.set(position, newData);
            notifyItemChanged(position);
        } else {
            add(newData);
        }
    }

    /**
     * 更新一条数据
     * 需要实现{@link DATA#equals(Object)}
     *
     * @param position
     * @param data
     */
    public synchronized void replace(int position, DATA data) {
        datas.set(position, data);
        notifyItemChanged(position);
    }

    /**
     * 替换一条数据
     * 需要实现{@link DATA#equals(Object)}
     *
     * @param oldData
     * @param newData
     */
    public synchronized void replace(DATA oldData, DATA newData) {
        int position = indexOfValue(oldData);
        if (position >= 0) {
            datas.set(position, newData);
            notifyItemChanged(position);
        }
    }


    /**
     * 删除一条item
     *
     * @param position
     */
    public synchronized void remove(int position) {
        datas.remove(position);
        notifyItemRemoved(position);
    }

    /**
     * 清除所有数据
     */
    public synchronized void clear() {
        datas.clear();
        notifyDataSetChanged();
    }

    /**
     * 移动一条数据
     *
     * @param from
     * @param to
     */
    public synchronized void moved(int from, int to) {
        Collections.swap(datas, from, to);
        notifyItemMoved(from, to);
    }

    /**
     * 倒叙
     */
    public synchronized void reverse() {
        Collections.reverse(datas);
        notifyItemMoved(0, datas.size());
    }

    /**
     * 数据总数
     *
     * @return
     */
    public synchronized final int size() {
        return datas.size();
    }


    /**
     * 某个位置的数据
     *
     * @param index
     * @return
     */
    public synchronized final DATA valueOfIndex(int index) {
        return datas.get(index);
    }

    /**
     * 某一条数据所在的位置
     *
     * @param data
     * @return <0 数据不存在；>=0 数据所在位置
     */
    public synchronized final int indexOfValue(DATA data) {
        if (data == null) {
            return -1;
        }
        for (int i = 0; i < datas.size(); i++) {
            if (areItemsTheSame(data, datas.get(i))) {
                return i;
            }
        }
        return -1;
    }

    public synchronized final List<DATA> getDatas() {
        return datas;
    }


    protected final boolean areItemsTheSame(DATA data1, DATA data2) {
        if (comparator != null) {
            return comparator.areItemsTheSame(data1, data2);
        }
        return data1.equals(data2);
    }

    protected final boolean areContentsTheSame(DATA data1, DATA data2) {
        if (comparator != null) {
            return comparator.areContentsTheSame(data1, data2);
        }
        return data1.equals(data2);
    }


    protected final void notifyDataSetChanged() {
        if (mRecycleAdapter != null) {
            mRecycleAdapter.notifyDataSetChanged();
        }
    }

    protected final void notifyItemInserted(int position) {
        if (mRecycleAdapter != null) {
            mRecycleAdapter.notifyItemInserted(position);
        }
    }

    protected final void notifyItemChanged(int position) {
        if (mRecycleAdapter != null) {
            mRecycleAdapter.notifyItemChanged(position);
        }
    }

    protected final void notifyItemRemoved(int position) {
        if (mRecycleAdapter != null) {
            mRecycleAdapter.notifyItemRemoved(position);
        }
    }

    protected final void notifyItemRangeInserted(int start, int count) {
        if (mRecycleAdapter != null) {
            mRecycleAdapter.notifyItemRangeInserted(start, count);
        }
    }

    protected final void notifyItemMoved(int from, int to) {
        if (mRecycleAdapter != null) {
            mRecycleAdapter.notifyItemMoved(from, to);
        }
    }


    /**
     * 数据对比器
     *
     * @param <DATA>
     */
    public interface Comparator<DATA> {
        /**
         * 比较两个item是否是同一个
         *
         * @param data1
         * @param data2
         * @return
         */
        boolean areItemsTheSame(DATA data1, DATA data2);

        /**
         * 比较同一个item内容是否一样
         *
         * @param data1
         * @param data2
         * @return
         */
        boolean areContentsTheSame(DATA data1, DATA data2);
    }

    private static class DiffCallback<DATA> extends DiffUtil.Callback {
        protected List<DATA> newdatas = null;
        protected List<DATA> olddatas = null;
        protected Comparator<DATA> comparator;

        private DiffCallback(List<DATA> newdatas, List<DATA> olddatas, Comparator<DATA> comparator) {
            if (newdatas != null && newdatas.size() > 0) {
                this.newdatas = new ArrayList<>(newdatas);
            }
            if (olddatas != null && olddatas.size() > 0) {
                this.olddatas = new ArrayList<>(olddatas);
            }
            this.comparator = comparator;
        }

        @Override
        public final int getOldListSize() {
            return olddatas == null ? 0 : olddatas.size();
        }

        @Override
        public final int getNewListSize() {
            return newdatas == null ? 0 : newdatas.size();
        }

        @Override
        public final boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
            DATA oldData = olddatas.get(oldItemPosition);
            DATA newData = newdatas.get(newItemPosition);
            if (comparator != null) {
                return comparator.areItemsTheSame(oldData, newData);
            } else {
                return oldData.equals(newData);
            }
        }

        @Override
        public final boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
            DATA oldData = olddatas.get(oldItemPosition);
            DATA newData = newdatas.get(newItemPosition);
            if (comparator != null) {
                return comparator.areContentsTheSame(oldData, newData);
            } else {
                return oldData.equals(newData);
            }
        }
    }

}
