package com.zxf.view;

import android.content.Context;
import android.util.DisplayMetrics;


public class DimenTools {
    private DimenTools() {
    }

    /**
     * @param context
     * @param value   原始像素
     * @return 与设备无关的像素
     */
    public static float px2dip(Context context, int value) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return value * metrics.density;
    }

    /**
     * @param context
     * @param value   与设备无关的像素
     * @return 原始像素
     */
    public static float dip2px(Context context, int value) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return value / metrics.density;
    }

    /**
     * @param context
     * @param value   原始像素
     * @return 按比例缩放的像素
     */
    public static float px2sp(Context context, int value) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return value * metrics.scaledDensity;
    }

    /**
     * @param context
     * @param value   按比例缩放的像素
     * @return 原始像素
     */
    public static float sp2px(Context context, int value) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return value / metrics.scaledDensity;
    }

    /**
     * px转毫米
     *
     * @param context
     * @param value   原始像素
     * @return 毫米
     */
    public static float px2mm(Context context, int value) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return value * metrics.xdpi * (1.0f / 25.4f);
    }

    /**
     * 毫米转px
     *
     * @param context
     * @param value   毫米
     * @return 原始像素
     */
    public static float mm2px(Context context, int value) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return value / (metrics.xdpi * (1.0f / 25.4f));
    }

    /**
     * @param context
     * @param value   原始像素
     * @return 以点为单位
     */
    public static float px2pt(Context context, int value) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return value * metrics.xdpi * (1.0f / 72);
    }

    /**
     * @param context
     * @param value   以点为单位
     * @return 原始像素
     */
    public static float pt2px(Context context, int value) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return value / (metrics.xdpi * (1.0f / 72));
    }


    /**
     * px转英寸
     *
     * @param context
     * @param value   原始像素
     * @return 英寸
     */
    public static float px2in(Context context, int value) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return value * metrics.xdpi;
    }

    /**
     * 英寸转px
     *
     * @param context
     * @param value   英寸
     * @return 原始像素
     */
    public static float in2px(Context context, int value) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return value / metrics.xdpi;
    }

}
