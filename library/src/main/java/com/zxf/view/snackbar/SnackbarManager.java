package com.zxf.view.snackbar;

import java.lang.ref.WeakReference;

class SnackbarManager {
    private WeakReference<Callback> callback;
    private static final SnackbarManager INSTANCE = new SnackbarManager();

    static SnackbarManager getInstance() {
        return INSTANCE;
    }

    void show(Callback callback) {
        Callback current = getCurrent();
        if (current != null) {
            current.dismiss(EasySnackbar.Callback.DISMISS_EVENT_CONSECUTIVE);
        }
        callback.show();
        this.callback = new WeakReference<>(callback);
    }

    void hide(Callback callback, int event) {
        Callback current = getCurrent();
        if (current != null && current == callback) {
            current.dismiss(event);
        }
    }

    private Callback getCurrent() {
        return this.callback == null ? null : this.callback.get();
    }

    interface Callback {
        void show();

        void dismiss(int event);
    }
}
