package com.zxf.view.snackbar;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.AnimRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.IdRes;
import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.zxf.fastlibrary.R;
import com.zxf.utils.MyLoopers;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class EasySnackbar {
    public interface Callback {

        /**
         * 滑动退出
         */
        public static final int DISMISS_EVENT_SWIPE = 0;
        /**
         * 点击按钮退出
         */
        public static final int DISMISS_EVENT_ACTION = 1;

        /**
         * 超时退出
         */
        public static final int DISMISS_EVENT_TIMEOUT = 2;
        /**
         * 调用{@link EasySnackbar#dismiss()}退出
         */
        public static final int DISMISS_EVENT_MANUAL = 3;
        /**
         * 有新的EasySnackbar显示时，旧的退出
         */
        public static final int DISMISS_EVENT_CONSECUTIVE = 4;

        @IntDef({
                DISMISS_EVENT_SWIPE, DISMISS_EVENT_ACTION, DISMISS_EVENT_TIMEOUT,
                DISMISS_EVENT_MANUAL, DISMISS_EVENT_CONSECUTIVE
        })
        @Retention(RetentionPolicy.SOURCE)
        public @interface DismissEvent {

        }

        void onDismissed(EasySnackbar snackbar, @DismissEvent int event);

        void onShown(EasySnackbar snackbar);
    }

    @IntDef({LENGTH_INDEFINITE, LENGTH_SHORT, LENGTH_LONG})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Duration {
    }

    /**
     * 显示Snackbar很长一段时间。
     */
    public static final int LENGTH_LONG = 0;
    /**
     * 在短时间显示EasySnackbar。
     */
    public static final int LENGTH_SHORT = -1;
    /**
     * 无限期地展示Snackbar。
     */
    public static final int LENGTH_INDEFINITE = -2;

    /**
     * 创建一个在顶部显示的Snackbar
     *
     * @param anchorView
     * @param text
     * @param duration
     * @return
     */
    public static EasySnackbar makeTop(View anchorView, CharSequence text, int duration) {
        EasySnackbar snackbar = make(anchorView, text, duration);
        snackbar.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL);
        snackbar.setAnimation(R.anim.top_in, R.anim.top_out);
        return snackbar;
    }

    /**
     * 创建一个在底部显示的Snackbar
     *
     * @param anchorView
     * @param text
     * @param duration
     * @return
     */
    public static EasySnackbar makeBottom(View anchorView, CharSequence text, int duration) {
        EasySnackbar snackbar = make(anchorView, text, duration);
        snackbar.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL);
        snackbar.setAnimation(R.anim.bottom_in, R.anim.bottom_out);
        return snackbar;
    }

    /**
     * 创建一个Snackbar
     *
     * @param anchorView
     * @param text
     * @param duration
     * @return
     */
    public static EasySnackbar make(View anchorView, CharSequence text, @Duration int duration) {
        EasySnackbar snackbar = new EasySnackbar(findSuitableParent(anchorView));
        snackbar.setDuration(duration);
        snackbar.setText(text);
        return snackbar;
    }


    public static EasySnackbar make(View anchorView, @StringRes int resId, @Duration int duration) {
        EasySnackbar snackbar = new EasySnackbar(findSuitableParent(anchorView));
        snackbar.setDuration(duration);
        snackbar.setText(resId);
        return snackbar;
    }


    public EasySnackbar(ViewGroup viewGroup) {
        mViewGroup = viewGroup;
        this.mSnackbarLayout = new SnackbarLayout(mViewGroup.getContext());
    }


    private boolean isShowing = false;
    private int duration = EasySnackbar.LENGTH_SHORT;
    private ViewGroup mViewGroup;
    private SnackbarLayout mSnackbarLayout;

    private static final int SHOW = 0;
    private static final int HIDE = 1;
    private static final int TIMEOUT = 2;

    private final Handler handler = new Handler(MyLoopers.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case SHOW:
                    mSnackbarLayout.show(mViewGroup, mManagerCallback);
                    if (mCallback != null) {
                        mCallback.onShown(EasySnackbar.this);
                    }
                    break;
                case HIDE:
                    mSnackbarLayout.remove();
                    if (mCallback != null) {
                        mCallback.onDismissed(EasySnackbar.this, (Integer) msg.obj);
                    }
                    break;
                case TIMEOUT:
                    SnackbarManager.getInstance().hide(mManagerCallback, EasySnackbar.Callback.DISMISS_EVENT_TIMEOUT);
                    break;
            }
        }
    };

    /**
     * 进入动画
     */
    @AnimRes
    private int animIn = R.anim.left_in;
    /**
     * 退出动画
     */
    @AnimRes
    private int animOut = R.anim.left_out;
    private Callback mCallback;

    public EasySnackbar setContentView(View contentView) {
        this.mSnackbarLayout.removeAllViews();
        this.mSnackbarLayout.addView(contentView);
        return this;
    }

    public TextView getTextView() {
        return mSnackbarLayout.getTextView();
    }

    public TextView getActionView() {
        return mSnackbarLayout.getActionView();
    }

    public EasySnackbar setText(CharSequence text) {
        this.mSnackbarLayout.getTextView().setText(text);
        return this;
    }

    public EasySnackbar setText(@StringRes int resId) {
        this.mSnackbarLayout.getTextView().setText(resId);
        return this;
    }

    public EasySnackbar setTextColor(int color) {
        this.mSnackbarLayout.getTextView().setTextColor(color);
        return this;
    }

    public EasySnackbar setActionColor(int color) {
        this.mSnackbarLayout.getActionView().setTextColor(color);
        return this;
    }

    public EasySnackbar setAction(String text, final View.OnClickListener listener) {
        this.mSnackbarLayout.getActionView().setText(text);
        this.mSnackbarLayout.getActionView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SnackbarManager.getInstance().hide(mManagerCallback, Callback.DISMISS_EVENT_ACTION);
                if (listener != null) {
                    listener.onClick(v);
                }
            }
        });
        return this;
    }

    /**
     * 设置背景
     *
     * @param drawableId
     * @return
     */
    public EasySnackbar setBackground(@DrawableRes int drawableId) {
        mSnackbarLayout.setBackgroundResource(drawableId);
        return this;
    }

    /**
     * 设置背景颜色
     *
     * @param color
     * @return
     */
    public EasySnackbar setBackgroundColor(int color) {
        mSnackbarLayout.setBackgroundColor(color);
        return this;
    }

    /**
     * 显示位置
     *
     * @param gravity
     */
    public EasySnackbar setGravity(int gravity) {
        mSnackbarLayout.setLayoutGravity(gravity);
        return this;
    }

    /**
     * 设置宽度
     *
     * @param width
     * @return
     */
    public EasySnackbar setWidth(int width) {
        mSnackbarLayout.setWidth(width);
        return this;
    }

    /**
     * 设置高度
     *
     * @param height
     * @return
     */
    public EasySnackbar setHeight(int height) {
        mSnackbarLayout.setHeight(height);
        return this;
    }

    public int getDuration() {
        return duration;
    }

    /**
     * 设置显示时间
     *
     * @param duration
     * @return
     */
    public EasySnackbar setDuration(@Duration int duration) {
        this.duration = duration;
        return this;
    }

    public EasySnackbar setMargins(int left, int top, int right, int bottom) {
        mSnackbarLayout.setMargins(left, top, right, bottom);
        return this;
    }

    /**
     * 设置进出动画
     *
     * @param animIn
     * @param animOut
     * @return
     */
    public EasySnackbar setAnimation(@AnimRes int animIn, @AnimRes int animOut) {
        this.animIn = animIn;
        this.animOut = animOut;
        return this;
    }

    public void setCallback(Callback mCallback) {
        this.mCallback = mCallback;
    }

    public <T extends View> T findViewById(@IdRes int id) {
        return mSnackbarLayout.findViewById(id);
    }

    public boolean isShowing() {
        return isShowing;
    }

    /**
     * 退出EasySnackbar
     */
    public void dismiss() {
        SnackbarManager.getInstance().hide(mManagerCallback, Callback.DISMISS_EVENT_MANUAL);
    }

    /**
     * 显示EasySnackbar
     */
    public void show() {
        SnackbarManager.getInstance().show(mManagerCallback);
    }

    private static ViewGroup findSuitableParent(View view) {
        ViewGroup fallback = null;
        do {
            if (view instanceof CoordinatorLayout) {
                return (ViewGroup) view;
            } else if (view instanceof FrameLayout) {
                if (view.getId() == android.R.id.content) {
                    return (ViewGroup) view;
                } else {
                    fallback = (ViewGroup) view;
                }
            }

            if (view != null) {
                final ViewParent parent = view.getParent();
                view = parent instanceof View ? (View) parent : null;
            }
        } while (view != null);
        return fallback;
    }

    private final SnackbarManager.Callback mManagerCallback = new SnackbarManager.Callback() {
        @Override
        public void show() {
            if (isShowing) {
                return;
            }
            Log.d("EasySnackbar", "show()");
            isShowing = true;
            handler.sendEmptyMessage(SHOW);
            Animation animation = AnimationUtils.loadAnimation(mViewGroup.getContext(), animIn);
            mSnackbarLayout.startAnimation(animation);
            if (duration == LENGTH_INDEFINITE) {
                return;
            }
            int delay = duration;
            if (delay == LENGTH_SHORT) {
                delay = 2000;
            } else if (delay == LENGTH_LONG) {
                delay = 5000;
            }
            handler.sendEmptyMessageDelayed(TIMEOUT, delay);
        }

        @Override
        public void dismiss(int event) {
            if (!isShowing) {
                return;
            }
            Log.d("EasySnackbar", "dismiss(" + event + ")");
            isShowing = false;
            handler.removeMessages(TIMEOUT);
            if (event == Callback.DISMISS_EVENT_CONSECUTIVE) {
                Message.obtain(handler, HIDE, event).sendToTarget();
                return;
            }
            Animation animation = AnimationUtils.loadAnimation(mViewGroup.getContext(), animOut);
            mSnackbarLayout.startAnimation(animation);
            handler.sendMessageDelayed(Message.obtain(handler, HIDE, event), 200);
        }
    };

    public static class FabTranslationBehavior extends CoordinatorLayout.Behavior<FloatingActionButton> {

        public FabTranslationBehavior() {
        }

        public FabTranslationBehavior(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        @Override
        public boolean layoutDependsOn(@NonNull CoordinatorLayout parent, @NonNull FloatingActionButton child, @NonNull View dependency) {
            return dependency instanceof SnackbarLayout;
        }

        private float transY = 0;

        @Override
        public boolean onDependentViewChanged(@NonNull CoordinatorLayout parent, @NonNull FloatingActionButton child, @NonNull View dependency) {
            if (dependency instanceof SnackbarLayout) {
                CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) dependency.getLayoutParams();
                if ((layoutParams.gravity & Gravity.BOTTOM) == Gravity.BOTTOM) {
                    float childBottom = child.getY() + child.getHeight();
                    float dependencyTop = dependency.getTop();
                    if (dependencyTop < childBottom) {
                        transY = dependencyTop - childBottom;
                        child.setTranslationY(transY);
                        return true;
                    }
                }
            }
            return super.onDependentViewChanged(parent, child, dependency);
        }

        @Override
        public void onDependentViewRemoved(@NonNull CoordinatorLayout parent, @NonNull FloatingActionButton child, @NonNull View dependency) {
            super.onDependentViewRemoved(parent, child, dependency);
            if (dependency instanceof SnackbarLayout) {
                CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) dependency.getLayoutParams();
                if ((layoutParams.gravity & Gravity.BOTTOM) == Gravity.BOTTOM) {
                    child.setTranslationY(child.getTranslationY() - transY);
                    transY = 0;
                }
            }
        }
    }

}
