package com.zxf.view.snackbar;

import android.content.Context;
import android.os.Build;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.widget.AutoSizeableTextView;

import com.google.android.material.behavior.SwipeDismissBehavior;
import com.zxf.fastlibrary.R;
import com.zxf.view.DimenTools;

class SnackbarLayout extends LinearLayout {
    private int width = ViewGroup.LayoutParams.WRAP_CONTENT;
    private int height = ViewGroup.LayoutParams.WRAP_CONTENT;
    private int gravity = Gravity.TOP;
    private int marginLeft, marginTop, marginRight, marginBottom;

    private TextView mTextView, mActionView;

    SnackbarLayout(Context context) {
        super(context);
        setOrientation(HORIZONTAL);
        setGravity(Gravity.CENTER_VERTICAL);
        setBackgroundResource(R.color.default_easysnackbar_background);
        int padding = (int) DimenTools.px2dip(context, 8);
        setPadding(padding, padding, padding, padding);
        mTextView = new TextView(context);
        LayoutParams layoutParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.weight = 1;
        mTextView.setLayoutParams(layoutParams);
        mTextView.setTextColor(getResources().getColor(R.color.default_easysnackbar_text));
        mTextView.setTextSize(18);
        addView(mTextView);

        mActionView = new TextView(context);
        mActionView.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        mActionView.setTextColor(getResources().getColor(R.color.default_easysnackbar_button));
        mActionView.setTextSize(16);
        addView(mActionView);
    }

    void setWidth(int width) {
        this.width = width;
    }

    void setHeight(int height) {
        this.height = height;
    }

    int getLayoutGravity() {
        return gravity;
    }

    void setLayoutGravity(int gravity) {
        this.gravity = gravity;
    }

    void setMargins(int left, int top, int right, int bottom) {
        this.marginLeft = left;
        this.marginTop = top;
        this.marginRight = right;
        this.marginBottom = bottom;
    }

    TextView getTextView() {
        return mTextView;
    }

    TextView getActionView() {
        return mActionView;
    }


    void show(ViewGroup mViewGroup, SnackbarManager.Callback mManagerCallback) {
        if (mViewGroup instanceof CoordinatorLayout) {
            CoordinatorLayout.LayoutParams layoutParams = new CoordinatorLayout.LayoutParams(width, height);
            final Behavior behavior = new Behavior();
            behavior.setManageCallback(mManagerCallback);
            layoutParams.setBehavior(behavior);
            layoutParams.gravity = gravity;
            layoutParams.setMargins(marginLeft, marginTop, marginRight, marginBottom);
            mViewGroup.addView(this, layoutParams);
        } else {
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(width, height);
            layoutParams.gravity = gravity;
            layoutParams.setMargins(marginLeft, marginTop, marginRight, marginBottom);
            mViewGroup.addView(this, layoutParams);
        }
    }

    void remove() {
        ViewGroup viewGroup = (ViewGroup) getParent();
        if (viewGroup != null) {
            viewGroup.removeView(this);
        }
    }


    static class Behavior extends SwipeDismissBehavior<SnackbarLayout> {
        public Behavior() {
            setStartAlphaSwipeDistance(0.1f);
            setEndAlphaSwipeDistance(0.6f);
            setSwipeDirection(SwipeDismissBehavior.SWIPE_DIRECTION_ANY);
            setListener(new OnDismissListener() {
                @Override
                public void onDismiss(View view) {
                    SnackbarManager.getInstance().hide(mCallback, EasySnackbar.Callback.DISMISS_EVENT_SWIPE);
                }

                @Override
                public void onDragStateChanged(int state) {
                    switch (state) {
                        case SwipeDismissBehavior.STATE_DRAGGING:
                        case SwipeDismissBehavior.STATE_SETTLING:
                            break;
                        case SwipeDismissBehavior.STATE_IDLE:
                            break;
                    }
                }
            });
        }

        private SnackbarManager.Callback mCallback;

        public void setManageCallback(SnackbarManager.Callback mCallback) {
            this.mCallback = mCallback;
        }

        @Override
        public boolean canSwipeDismissView(View child) {
            return child instanceof SnackbarLayout;
        }
    }
}
