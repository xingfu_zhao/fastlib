package com.zxf.view;

public class ClickOn {
    private static long lastTime = 0;
    private static long intervalTime = 200;

    /**
     * 是否是快速点击多次
     * @return
     */
    public static boolean isQuickClick() {
        return isQuickClick(intervalTime);
    }
    /**
     * 是否是快速点击多次
     * @param interval 点击间隔时间
     * @return
     */
    public static boolean isQuickClick(long interval) {
        long clickTime = System.currentTimeMillis();
        long d = clickTime - lastTime;
        if (interval < 0) {
            interval = intervalTime;
        }
        if (d < interval && d > 0) {
            return true;
        }
        lastTime = clickTime;
        return false;
    }



}
